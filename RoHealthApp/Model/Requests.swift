//
//  PlanModel.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/25/21.
//

import SwiftUI

struct PlanModel: Codable, Identifiable {
    let id: String
    let name: String
    let image: String
    let covers: String
    let nameColor: String
    let hospital: String
    let amount: String
    let planCover: [String]
    let allPlanCover: [String]
}

struct PhoneNumberVerificationReq {
    var mobile: String?
}

struct StaffLoginReq{
    var email, password: String?
}

struct StaffRegistrationReq{
    var firstName, lastName, email, address: String?
    var gender, maritalStatus, state, country: String?
    var prevCondition, pin, mobile: String?
    
    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case email, address, gender
        case maritalStatus = "marital_status"
        case state, country
        case prevCondition = "prev_condition"
        case pin, mobile
    }
}

struct OTPVerificationReq {
    var otp: String?
}

struct SetNewPasswordReq {
    var pin, cpin: String?
}

struct AddDependantsReq {
    var type, firstName, lastName, mobile: String?
    
    enum CodingKeys: String, CodingKey {
        case type
        case firstName = "first_name"
        case lastName = "last_name"
        case mobile
    }
}
