//
//  ViewModel.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/18/21.
//

import SwiftUI

class ViewModel: ObservableObject {
    //MARK: - Properties
    let userDefaults = UserDefaults.standard
    var refId = ""
    
    //Phone number verification variables
    @Published var phoneNumberReq = PhoneNumberVerificationReq()
    @Published var verificationCode = 0
    @Published var verificationMessage = ""
    
    //OTP Verification Variables
    @Published var otpReq = OTPVerificationReq()
    @Published var otpCode = 0
    @Published var otpMessage = ""
    
    //Signup variables
    @Published var signupReq = StaffRegistrationReq()
    @Published var signupCode = 0
    @Published var signupMessage = ""
    @Published var signupResp = RegistrationUserResp()
    
    //Login Variables
    @Published var loginReq = StaffLoginReq()
    @Published var loginCode = 0
    @Published var loginMessage = ""
    
    //Forgot Password Variables
    @Published var forgotPasswordReq = PhoneNumberVerificationReq()
    @Published var forgotPasswordCode = 0
    @Published var forgotPasswordMess = ""
    @Published var forgotPasswordRef = ""
    
    //Reset Password Variables
    @Published var resetPasswordReq = SetNewPasswordReq()
    @Published var resetPasswordCode = 0
    @Published var resetPasswordMess = ""
    
     //MARK: - Add Beneficiary
    @Published var addBeneficiaryReq = AddDependantsReq()
    @Published var addBeneficiaryCode = 0
    @Published var addBeneficiaryMess = ""
    @Published var addBeneficiaryData: [Dependent] = []
    
     //MARK: - Get Beneficiaries Variables
    @Published var getBenefiaciaryCode = 0
    @Published var getBeneficiaryMess = ""
    @Published var getBeneficiaryData: [Dependent] = []
    @Published var getBeneficiaryObject = Dependent()
    
     //MARK: - Confirm Beneficiaries
    @Published var confirmBenCode = 0
    @Published var confirmBenMess = ""
    
     //MARK: - Get New Beneficiaries Variables
    @Published var getNewBenCode = 0
    @Published var getNewBenMess = ""
    @Published var getNewBenArray: [Dependent] = []
    
     //MARK: - Get Packages Variables
    @Published var getPackagesCode = 0
    @Published var getPackagesMess = ""
    @Published var getPackagesData: [GetPackagesDetails] = []
    @Published var packages = GetPackagesDetails()
    @Published var budget = ""
    
     //MARK: - Purchase Variables
    @Published var purchasePackageCode = 0
    @Published var purchasePackageMess = ""
    
     //MARK: - GET USER DETAILS
    @Published var getUserDetailsCode = 0
    @Published var getUserDetailsMess = ""
    @Published var getUserDetails = UserDetailsEntity()
    
     //MARK: - DELETE BENEFICIARY
    @Published var deleteUserCode = 0
    @Published var deleteUserMess = ""
    
     //MARK: - LOGOUT
    @Published var isLoggedIn = false
    @Published var logoutCode = 0
    @Published var logoutMess = ""
    
     //MARK: - Get Hospital
    @Published var getHospitalsMess = ""
    @Published var getHospitalsCode = 0
    @Published var getHospitalData: [HospitalEntity] = []
    
     //MARK: - Get Transactions Variables
    @Published var getTransactionsMess = ""
    @Published var getTransactionsCode = 0
    @Published var getTransactionsData: [Transaction] = []
    
    //Initialization
    init() {
//        verifyPhoneNumber(phoneNumber: String())
//        verifyOTP(otp: String())
//        signupStaff(firstName: String(), email: String(), gender: String(), pin: String(), address: String(), country: String(), maritalStatus: String(), state: String(), lastName: String(), prevCondition: String())
//        loginStaff(email: String(), password: String())
//        verifyResetPasswordOTP(otp: String())
//        forgotPassword(phoneNumber: String())
//        setNewPassword(password: String(), confirmPassword: String())
        getNewBeneficiary()
        getBeneficiaries()
        fetchUserDetails()
        loadHospital()
        loadTransactions()
    }
    
    //Verify phone number function
    func verifyPhoneNumber(phoneNumber: String) {
        phoneNumberReq.mobile = phoneNumber
        NetworkClass.shared.verifyPhoneNumber(requestModel: phoneNumberReq) { (feedback) in
            self.verificationCode = feedback.status?.code ?? Int()
            self.verificationMessage = feedback.status?.desc ?? String()
            for item in feedback.entity?.entity ?? [PhoneNumberEntity]() {
                self.userDefaults.set(item.referenceID ?? String(), forKey: "refID")
            }
            print("Something")
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
    //OTP verification function
    func verifyOTP(otp: String) {
        otpReq.otp = otp
        NetworkClass.shared.verifyOTP(requestModel: otpReq) { (feedback) in
            self.otpCode = feedback.status?.code ?? Int()
            self.otpMessage = feedback.status?.desc ?? String()
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
    //Signup function
    func signupStaff(firstName: String, email: String, gender: String, pin: String, address: String, country: String, maritalStatus: String, state: String, lastName: String, prevCondition: String, mobile: String) {
        signupReq.address = address
        signupReq.firstName = firstName
        signupReq.lastName = lastName
        signupReq.prevCondition = prevCondition
        signupReq.email = email
        signupReq.pin = pin
        signupReq.gender = gender
        signupReq.country = country
        signupReq.state = state
        signupReq.mobile = mobile
        signupReq.maritalStatus = maritalStatus
        NetworkClass.shared.signUp(requestModel: signupReq) { (feedback) in
            self.signupCode = feedback.status?.code ?? Int()
            self.signupMessage = feedback.status?.desc ?? String()
            self.signupResp = feedback.entity?.user ?? RegistrationUserResp()
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
    //Login Function
    func loginStaff(email: String, password: String) {
        loginReq.email = email
        loginReq.password = password
        NetworkClass.shared.loginUser(requestModel: loginReq) { (feedback) in
            self.loginCode = feedback.status?.code ?? Int()
            self.loginMessage = feedback.status?.desc ?? String()
            self.userDefaults.set(feedback.entity?.token ?? String(), forKey: "token")
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
     //MARK: - Forgot Password
    func forgotPassword(phoneNumber: String) {
        forgotPasswordReq.mobile = phoneNumber
        NetworkClass.shared.verifyPhoneNumber(requestModel: forgotPasswordReq) { (feedback) in
            self.forgotPasswordCode = feedback.status?.code ?? Int()
            self.forgotPasswordMess = feedback.status?.desc ?? String()
            for item in feedback.entity?.entity ?? [PhoneNumberEntity]() {
                self.forgotPasswordRef = item.referenceID ?? String()
                self.refId = item.referenceID ?? String()
                self.userDefaults.set(item.referenceID ?? String(), forKey: "resetID")
            }
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
     //MARK: - Verify Reset Password OTP
    func verifyResetPasswordOTP(otp: String) {
        otpReq.otp = otp
        NetworkClass.shared.verifyResetPasswordOTP(requestModel: otpReq) { (feedback) in
            self.otpCode = feedback.status?.code ?? Int()
            self.otpMessage = feedback.status?.desc ?? String()
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
     //MARK: - Set New Password
    func setNewPassword(password: String, confirmPassword: String) {
        resetPasswordReq.pin = password
        resetPasswordReq.cpin = confirmPassword
        NetworkClass.shared.resetPassword(requestModel: resetPasswordReq) { (feedback) in
            self.resetPasswordCode = feedback.status?.code ?? Int()
            self.resetPasswordMess = feedback.status?.desc ?? String()
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
     //MARK: - Add Beneficiary
    func addBeneficiary(firstName: String, lastName: String, phoneNumber: String, type: String) {
        addBeneficiaryReq.firstName = firstName
        addBeneficiaryReq.lastName = lastName
        addBeneficiaryReq.type = type
        addBeneficiaryReq.mobile = phoneNumber
        NetworkClass.shared.addDependant(requestModel: addBeneficiaryReq) { (feedback) in
            self.addBeneficiaryCode = feedback.status?.code ?? Int()
            self.addBeneficiaryMess = feedback.status?.desc ?? String()
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
    //MARK: - Get New Beneficiary
    func getNewBeneficiary() {
        NetworkClass.shared.getNewBeneficiary { (feedback) in
            self.getNewBenCode = feedback.status?.code ?? Int()
            self.getNewBenMess = feedback.status?.desc ?? String()
            self.getNewBenArray = feedback.entity?.dependents ?? [Dependent]()
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
     //MARK: - Confirm Beneficiaries
    func confirmBeneficiaries() {
        NetworkClass.shared.confirmBeneficiaries { (feedback) in
            self.confirmBenCode = feedback.status?.code ?? Int()
            self.confirmBenMess = feedback.status?.desc ?? String()
            print(feedback.status?.desc ?? String(), ">>")
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
     //MARK: - Get Beneficiaries
    func getBeneficiaries() {
        NetworkClass.shared.fetchBeneficiaries { (feedback) in
            self.getBenefiaciaryCode = feedback.status?.code ?? Int()
            self.getBeneficiaryMess = feedback.status?.desc ?? String()
            self.getBeneficiaryData = feedback.entity?.dependents ?? [Dependent]()
            print(feedback.status?.code ?? Int(), feedback.entity?.dependents ?? [Dependent](), feedback.status?.desc ?? String(), "???")
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
     //MARK: - Get Packages
    func getPackages() {
        NetworkClass.shared.fetchPackages { (feedback) in
            self.getPackagesCode = feedback.status?.code ?? Int()
            self.getPackagesMess = feedback.status?.desc ?? String()
            self.getPackagesData = feedback.entity?.packages ?? [GetPackagesDetails]()
            self.budget = feedback.entity?.budget ?? String()
            print(feedback.entity?.packages ?? [GetPackagesDetails]())
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
     //MARK: - Purchase Package
    func purchasePackage(id: Int) {
        NetworkClass.shared.purchasePackage(id: id) { (feedback) in
            self.purchasePackageCode = feedback.status?.code ?? Int()
            self.purchasePackageMess = feedback.status?.desc ?? String()
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
     //MARK: - GET USER DETAILS
    func fetchUserDetails() {
        NetworkClass.shared.getUserDetails { (feedback) in
            self.getUserDetailsCode = feedback.status?.code ?? Int()
            self.getUserDetailsMess = feedback.status?.desc ?? String()
            self.getUserDetails = feedback.entity ?? UserDetailsEntity()
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
     //MARK: - DELETE BENEFICIARY METHOD
    func deleteBeneficiary(id: Int) {
        NetworkClass.shared.deleteBeneficiary(id: id) { (feedback) in
            self.deleteUserCode = feedback.status?.code ?? Int()
            self.deleteUserMess = feedback.status?.desc ?? String()
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
     //MARK: - Logout Function
    func logout() {
        NetworkClass.shared.logoutUser { (feedback) in
            self.logoutCode = feedback.status?.code ?? Int()
            self.logoutMess = feedback.status?.desc ?? String()
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
     //MARK: - GET HOSPITAL
    func loadHospital() {
        NetworkClass.shared.loadHospitals { (feedback) in
            self.getHospitalsCode = feedback.status?.code ?? Int()
            self.getHospitalsMess = feedback.status?.desc ?? String()
            self.getHospitalData = feedback.entity ?? [HospitalEntity]()
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
    
     //MARK: - Get Transactions Function
    func loadTransactions() {
        NetworkClass.shared.getTransactions { (feedback) in
            self.getTransactionsCode = feedback.status?.code ?? Int()
            self.getTransactionsMess = feedback.status?.desc ?? String()
            self.getTransactionsData = feedback.entity?.transactions ?? [Transaction]()
        } failure: { (error) in
            print("Error: \(error.debugDescription)")
        }
    }
}
