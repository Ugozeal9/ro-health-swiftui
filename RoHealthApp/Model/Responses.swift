//
//  Responses.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/17/21.
//

import Foundation

struct GenericResponse: Codable {
    var status: GenericStatusResponse?
}

struct GenericStatusResponse: Codable {
    var code: Int?
    var desc: String?
}

struct ResistrationResp: Codable {
    var status: RegistrationStatusResp?
    var entity: RegistrationEntityResp?
}

struct RegistrationStatusResp: Codable {
    var code: Int?
    var desc: String?
}

struct RegistrationEntityResp: Codable {
    var token: String?
    var exp: Int?
    var user: RegistrationUserResp?
}

struct RegistrationUserResp: Codable {
    var created: String?
    var updated, company: String?
    var firstName: String?
    var lastName, dob, gender, mobile: String?
    var email, password, address: String?
    var state, city, country, jobPosition: String?
    var staffNumber: String?
    var budget: String?
    var status, id: Int?

    enum CodingKeys: String, CodingKey {
        case created, updated, company
        case firstName = "first_name"
        case lastName = "last_name"
        case dob, gender, mobile, email, password, address, state, city, country
        case jobPosition = "job_position"
        case staffNumber = "staff_number"
        case budget, status
        case id = "_id"
    }
}

struct LoginResp: Codable {
    var status: LoginStatusResp?
    var entity: LoginEntityResp?
}

struct LoginStatusResp: Codable {
    var code: Int?
    var desc: String?
}

struct LoginEntityResp: Codable {
    var token: String?
    var exp: Int?
}

struct LoginUserResp: Codable {
    var created: String?
    var updated: String?
    var company: UserCompany?
    var firstName: String?
    var lastName, dob, gender, mobile: String?
    var email, password, address: String?
    var state, city, country, jobPosition: String?
    var staffNumber: String?
    var budget: UserBudget?
    var status, id: Int?

    enum CodingKeys: String, CodingKey {
        case created, updated, company
        case firstName = "first_name"
        case lastName = "last_name"
        case dob, gender, mobile, email, password, address, state, city, country
        case jobPosition = "job_position"
        case staffNumber = "staff_number"
        case budget, status
        case id = "_id"
    }
}

struct UserCompany: Codable {
    var created: String?
    var updated: String?
    var companyName: String?
    var email: String?
    var mobile: String?
    var regNo, address, city, state: String?
    var country: String?
    var status: Int?
    var balance: String?
    var accountNumber, walletID, users, hospitals: String?
    var id: Int?
    
    enum CodingKeys: String, CodingKey {
        case created, updated
        case companyName = "company_name"
        case email, mobile
        case regNo = "reg_no"
        case address, city, state, country, status, balance
        case accountNumber = "account_number"
        case walletID = "wallet_id"
        case users, hospitals
        case id = "_id"
    }
}

struct UserBudget: Codable {
    var created: String?
    var updated, lastDate: String?
    var dueDate: String?
    var company, employee: Int?
    var budget: String?
    var status: Int?
    var amount: String?
    var id: Int?
    
    enum CodingKeys: String, CodingKey {
        case created, updated
        case lastDate = "last_date"
        case dueDate = "due_date"
        case company, employee, budget, status, amount
        case id = "_id"
    }
}

struct PhoneNumberVerificationResp: Codable {
    var status: PhoneNumberVerificationStatusResp?
    var entity: PhoneNumberEntityResp?
}

struct PhoneNumberVerificationStatusResp: Codable {
    var code: Int?
    var desc: String?
}

struct PhoneNumberEntityResp: Codable {
    var entity: [PhoneNumberEntity]?
}

struct PhoneNumberEntity: Codable {
    var referenceID, destination, statusID, status: String?
    
    enum CodingKeys: String, CodingKey {
        case referenceID = "reference_id"
        case destination
        case statusID = "status_id"
        case status
    }
}



struct OTPValidationResp: Codable {
    var status: OTPValidationStatusResp?
}

struct OTPValidationStatusResp: Codable {
    var code: Int?
    var desc: String?
}

struct ConfirmPasswordResp: Codable {
    var status: ConfirmPasswordStatusResp?
}

struct ConfirmPasswordStatusResp: Codable {
    var code: Int?
    var desc: String?
}

struct GetBeneficiariesResp: Codable {
    var status: GetBeneficiaryStatus?
    var entity: GetBeneficiaryEntity?
}

struct GetBeneficiaryStatus: Codable {
    var code: Int?
    var desc: String?
}

struct GetBeneficiaryEntity: Codable {
    var dependents: [Dependent]?
}

struct Dependent: Codable, Hashable {
    var type: String?
    var firstName, lastName, mobile: String?
    var id: Int?
    
    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case mobile
        case type
        case id
    }
}

struct GetPackagesResp: Codable {
    var status: GenericStatusResponse?
    var entity: GetPackagesEntity?
}

struct GetPackagesEntity: Codable {
    var budget: String?
    var packages: [GetPackagesDetails]?
}

struct GetPackagesDetails: Codable, Identifiable {
    var name, price: String?
    var detailDescription: [String]?
    var hospital: Int?
    var id: Int?
    
    enum CodingKeys: String, CodingKey {
        case name
        case detailDescription = "description"
        case price, hospital = "no_of_hospitals"
        case id
    }
}

struct FetchSinglePackage: Codable {
    var status: GenericStatusResponse?
    var entity: SinglePackageEntity?
}

struct SinglePackageEntity: Codable {
    
}

struct ConfirmPurchaseResp: Codable {
    var status: GenericStatusResponse?
    var entity: ConfirmPurchaseEntity?
}

struct ConfirmPurchaseEntity: Codable {
    var plan, purchaseDate, amount: String?
    
    enum CodingKeys: String, CodingKey {
        case plan
        case purchaseDate = "purchase_date"
        case amount
    }
}

struct GetUserDetailsResp: Codable {
    var status: GenericStatusResponse?
    var entity: UserDetailsEntity?
}

struct UserDetailsEntity: Codable {
    var id: Int?
    var dateCreated, lastLogin, firstName, lastName: String?
    var email, phoneNo, address, gender: String?
    var budget: String?
    var healthPlan: UserHealthPlan?

    enum CodingKeys: String, CodingKey {
        case id
        case dateCreated = "date_created"
        case lastLogin = "last_login"
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case phoneNo = "phone_no"
        case address, gender, budget
        case healthPlan = "health_plan"
    }
}

struct UserHealthPlan: Codable{
    var name, price: String?
    var hmo: String?
    var healthPlanDescription: [String]?
    
    enum CodingKeys: String, CodingKey {
        case name, price, hmo
        case healthPlanDescription = "description"
    }
}

struct GetAllHospitalsResp: Codable {
    var status: GenericStatusResponse?
    var entity: [HospitalEntity]?
}

struct HospitalEntity: Codable, Identifiable {
    var id: Int?
    var name, address, email, mobile: String?
    var state: String?
}

struct GetTransactionsResp: Codable {
    var status: GenericStatusResponse?
    var entity: TransactionEntity?
}

struct TransactionEntity: Codable {
    var transactions: [Transaction]?
}

class Transaction: Codable, Hashable {
    static func == (lhs: Transaction, rhs: Transaction) -> Bool {
        return lhs.amount == rhs.amount && lhs.created == rhs.created && lhs.type == rhs.type && lhs.info == rhs.info && lhs.status == rhs.status
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(created)
    }
    
    var created, type, amount, info: String?
    var status: Int?
}
