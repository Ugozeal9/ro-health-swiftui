//
//  AccountView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/2/21.
//

import SwiftUI

struct AccountView: View {
    @AppStorage("isNotification") var isNotification: Bool = false
    @StateObject var viewModel = ViewModel()
    @State var isSignedIn = false
    @AppStorage("isLoggedIn") var isLoggedIn: Bool = false
    
    let haptics = UIImpactFeedbackGenerator(style: .medium)
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                ZStack {
                    Image("account-bg")
                        .resizable()
                        .scaledToFill()
                        .frame(width: 250)
                    HStack(alignment: .center, spacing: 10) {
                        Image("test-image")
                            .resizable()
                            .scaledToFill()
                            .frame(width:70, height: 70)
                            .clipShape(
                                Circle()
                            )
                        VStack(alignment: .leading, spacing: 8) {
                            Text("\(viewModel.getUserDetails.firstName ?? String()) \(viewModel.getUserDetails.lastName ?? String())")
                                .font(.custom("Nunito-SemiBold", size: 14))
                                .foregroundColor(.white)
                            Text(viewModel.getUserDetails.email ?? String())
                                .font(.custom("Nunito-SemiBold", size: 12))
                                .foregroundColor(Color(.systemGray2))
                        }
                        Spacer()
                        VStack {
                            Image(systemName: "highlighter")
                                .foregroundColor(.white)
                            Spacer()
                        }
                    }
                    .padding(.vertical)
                }//:ZSTACK
                .frame(height: 100)
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.clear, lineWidth: 1)
                )
                .background(RoundedRectangle(cornerRadius: 10).fill(Color("rov-blue")))
                .padding()
                Divider()
                    .padding()
                VStack {
                    NavigationLink(
                        destination: EmptyView(),
                        label: {
                            HStack(alignment: .center, spacing: 11) {
                                HStack {
                                    Image(systemName:"person.fill")
                                        .resizable()
                                        .scaledToFit()
                                        .foregroundColor(Color("rov-blue"))
                                }
                                .frame(width: 24, height: 24)
                                .padding()
                                .overlay(
                                    RoundedRectangle(cornerRadius: 10)
                                        .stroke(Color.clear, lineWidth: 1)
                                )
                                .background(RoundedRectangle(cornerRadius: 10).fill(Color("budget-Color")))
                                
                                Text("My Profile")
                                    .foregroundColor(Color("textColor"))
                                    .font(.custom("Nunito-SemiBold", size: 13))
                                Spacer()
                            }
                            .padding(.horizontal)
                        })
                    Divider()
                        .padding()
                    HStack(alignment: .center, spacing: 11) {
                        HStack {
                            Image(systemName:"bell.fill")
                                .resizable()
                                .scaledToFit()
                                .foregroundColor(Color("rov-blue"))
                        }
                        .frame(width: 24, height: 24)
                        .padding()
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color.clear, lineWidth: 1)
                        )
                        .background(RoundedRectangle(cornerRadius: 10).fill(Color("budget-Color")))
                        
                        Text("Notification")
                            .foregroundColor(Color("textColor"))
                            .font(.custom("Nunito-SemiBold", size: 13))
                        Spacer()
                        Toggle(isOn: $isNotification, label: {
                            if isNotification {
                                
                            } else {
                                
                            }
                        })
                        .toggleStyle(SwitchToggleStyle(tint: Color("rov-blue")))
                    }
                    .padding(.horizontal)
                    Divider()
                        .padding()
                    //                    NavigationLink(
                    //                        destination: UpgradeBudgetView(),
                    //                        label: {
                    //                            HStack(alignment: .center, spacing: 11) {
                    //                                HStack {
                    //                                    Image(systemName:"briefcase.fill")
                    //                                        .resizable()
                    //                                        .scaledToFit()
                    //                                        .foregroundColor(Color("rov-blue"))
                    //                                }
                    //                                .frame(width: 24, height: 24)
                    //                                .padding()
                    //                                .overlay(
                    //                                    RoundedRectangle(cornerRadius: 10)
                    //                                        .stroke(Color.clear, lineWidth: 1)
                    //                                )
                    //                                .background(RoundedRectangle(cornerRadius: 10).fill(Color("budget-Color")))
                    //
                    //                                Text("Upgrade Plan")
                    //                                    .foregroundColor(Color("textColor"))
                    //                                    .font(.custom("Nunito-SemiBold", size: 13))
                    //                                Spacer()
                    //                            }
                    //                            .padding(.horizontal)
                    //                        })
                    //                    Divider()
                    //                        .padding()
                    NavigationLink(
                        destination: TransactionHistory(),
                        label: {
                            HStack(alignment: .center, spacing: 11) {
                                HStack {
                                    Image("wallet")
                                        .resizable()
                                        .scaledToFit()
                                        .foregroundColor(Color("rov-blue"))
                                }
                                .frame(width: 24, height: 24)
                                .padding()
                                .overlay(
                                    RoundedRectangle(cornerRadius: 10)
                                        .stroke(Color.clear, lineWidth: 1)
                                )
                                .background(RoundedRectangle(cornerRadius: 10).fill(Color("budget-Color")))
                                
                                Text("Transactions")
                                    .foregroundColor(Color("textColor"))
                                    .font(.custom("Nunito-SemiBold", size: 13))
                                Spacer()
                            }
                            .padding(.horizontal)
                        })
                    Divider()
                        .padding()
                    
                    Button(action: {
                        viewModel.logout()
                        let delay = DispatchTime.now() + .milliseconds(700)
                        DispatchQueue.main.asyncAfter(deadline: delay) {
                            if viewModel.logoutCode == 100 {
                                self.isSignedIn = true //trigger NavigationLink
                            }
                        }
                    }, label: {
                        HStack(alignment: .center, spacing: 11) {
                            HStack {
                                Image("logout")
                                    .resizable()
                                    .scaledToFit()
                                    .foregroundColor(Color("rov-blue"))
                            }
                            .frame(width: 24, height: 24)
                            .padding()
                            .overlay(
                                RoundedRectangle(cornerRadius: 10)
                                    .stroke(Color.clear, lineWidth: 1)
                            )
                            .background(RoundedRectangle(cornerRadius: 10).fill(Color("budget-Color")))
                            
                            Text("Log Out")
                                .foregroundColor(Color("textColor"))
                                .font(.custom("Nunito-SemiBold", size: 13))
                            Spacer()
                        }
                        .padding(.horizontal)
                    })
                    .fullScreenCover(isPresented: $isSignedIn, content: {
                        LoginView()
                    })
                    .simultaneousGesture(TapGesture().onEnded{
                        haptics.impactOccurred()
                    })
                    
                    Divider()
                        .padding()
                }//:VSTACK
                .navigationBarTitle("My Account")
                .navigationBarBackButtonHidden(true)
            }
            
        }//:SCROLLVIEW
        .navigationBarHidden(true)
    }
}

struct AccountView_Previews: PreviewProvider {
    static var previews: some View {
        AccountView()
    }
}
