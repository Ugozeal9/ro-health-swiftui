//
//  PlanView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/2/21.
//

import SwiftUI

struct PlanView: View {
    let houseImages: [UIImage] = [UIImage(named: "card-image")!]
    @StateObject var viewModel = ViewModel()
    @State var isViewAllClicked = false
    
    var body: some View {
        ZStack {
            VStack (spacing: 0) {
                LoginTopView(loginData: viewModel.getUserDetails)
                    .padding(.top, UIApplication.shared.windows.first?.safeAreaInsets.top)
                Divider()
                    .padding()
                
                ScrollView(.vertical, showsIndicators: false) {
                    HStack {
                        VStack(alignment: .leading) {
                            Text("HMO CARDS")
                                .foregroundColor(Color("textColor"))
                                .font(.custom("Nunito-SemiBold", size: 14))
                            Text("for you and your dependents")
                                .foregroundColor(Color("textColor"))
                                .font(.custom("Nunito-SemiBold", size: 7))
                        }
                        Spacer()
                        //                    Button(action: {
                        //                        print("UPGRADE")
                        //                    }, label: {
                        //                        Text("Upgrade Budget")
                        //                            .foregroundColor(Color("rov-blue"))
                        //                            .font(.custom("Nunito-SemiBold", size: 10))
                        //                    })
                    }
                    .padding()
                    
                    HMOCardItem(cardImage: houseImages)
                        .frame(width: 300, height: 190)
                    Divider()
                        .padding()
                    
                    HStack {
                        Text("Plan Covers: ")
                            .underline()
                            .foregroundColor(Color("textColor"))
                            .font(.custom("Nunito-SemiBold", size: 14))
                        Spacer()
                        Button(action: {
                            print("View all")
                        }) {
                            Text("View All")
                                .foregroundColor(Color("rov-blue"))
                                .font(.custom("Nunito-SemiBold", size: 11))
                        }
                    }
                    .padding()
                    UserPlanCoverItem(plan: viewModel.getUserDetails)
                    
                    Divider()
                        .padding()
                    HStack {
                        Text("Hospitals covered by your Plan")
                            .underline()
                            .foregroundColor(Color("textColor"))
                            .font(.custom("Nunito-SemiBold", size: 14))
                        Spacer()
                        NavigationLink(
                            destination: SelectHospitalView(),
                            isActive: self.$isViewAllClicked,
                            label: {
                                Button(action: {
                                    print("View all")
                                    let delay = DispatchTime.now() + .milliseconds(700)
                                    DispatchQueue.main.asyncAfter(deadline: delay) {
                                        if viewModel.getHospitalsCode == 100 {
                                            self.isViewAllClicked = true
                                        }
                                    }
                                }) {
                                    Text("View All")
                                        .foregroundColor(Color("rov-blue"))
                                        .font(.custom("Nunito-SemiBold", size: 11))
                                }
                            })
                    }
                    .padding()
                    
                    ForEach(viewModel.getHospitalData) { item in
                        HospitalListItem(hospital: item)
                            .background(Rectangle().fill(Color.white).shadow(radius: 2))
                            Divider()
                        }
                        .padding(.horizontal)
                    
                    Spacer()
//                    HStack(alignment: .center, spacing: 16) {
//                        Image("gold-image")
//                            .resizable()
//                            .scaledToFill()
//                            .frame(width:50, height: 50)
//                            .clipShape(
//                                RoundedRectangle(cornerRadius: 10)
//                            )
//                            .padding(.horizontal, 2)
//                        VStack(alignment: .leading, spacing: 3) {
//                            Text("XYZ Hospital")
//                                .foregroundColor(Color("textColor"))
//                                .font(.custom("Nunito-SemiBold", size: 14))
//                            Text("256, Murtala Muhammed Way, Alagomeji, Yaba")
//                                .foregroundColor(Color("textColor"))
//                                .font(.custom("Nunito-SemiBold", size: 10))
//                            Text("rate your last visit")
//                                .foregroundColor(Color(.systemGray2))
//                                .font(.custom("Nunito-SemiBold", size: 10))
//                        }
                      
//                    }
//                    .padding()
//                    .background(Rectangle().fill(Color.white).shadow(radius: 2))
//                    .padding(.horizontal)
                    
                }
            }
            .navigationBarTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
        }//:ZSTACK
        .ignoresSafeArea(.all, edges: .all)
    }
}

struct PlanView_Previews: PreviewProvider {
    static var previews: some View {
        PlanView()
            .previewDevice("iPhone 12 Pro")
    }
}
