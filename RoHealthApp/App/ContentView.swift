//
//  ContentView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/20/21.
//

import SwiftUI

struct ContentView: View {
    @State var isRegistered = false
    @State var isNotRegistered = false
    @StateObject var viewModel = ViewModel()
   
    var body: some View {
        NavigationView {
            VStack {
                TopView()
                    .frame(height: 170)
                    .edgesIgnoringSafeArea(.all)
                    .padding(EdgeInsets(top: 0, leading: 0, bottom: -70, trailing: 0))
                Section {
                    Text("Welcome To RoHealth App")
                        .font(.custom(K.Fonts.semiBold, fixedSize: 18))
                        .foregroundColor(.black)
                }
                
                //MARK: - NAVIGATION LINKS
                Section {
                    NavigationLink(destination: StartVerificationView(), isActive: $isNotRegistered) {
                        DefaultButtonView(buttonText: "Click here if you Were invited by your employer", textColor: .gray, backgroundColor: .white, borderColor: .gray)
                    }
                }
                
                Section {
                    NavigationLink(destination: LoginView(), isActive: $isRegistered) {
                        DefaultButtonView(buttonText: "Returning User? Login Here", textColor: .gray, backgroundColor: .white, borderColor: .gray)
                    }
                }
                Spacer()
            }
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .environmentObject(viewModel)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .previewDevice("iPhone 7")
    }
}
