//
//  DependantsView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/2/21.
//

import SwiftUI

struct DependantsView: View {
    let hapticImpact = UIImpactFeedbackGenerator(style: .medium)
    @ObservedObject var viewModel = ViewModel()
    @State var canAddDependants = false
    @State var dependentId = 0
    @State private var shouldShowLoginAlert: Bool = false
    var dependant = Dependent()
    
    func delete(at offsets: IndexSet) {
       
        let delay = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: delay) {
            viewModel.deleteBeneficiary(id: dependentId)
            viewModel.getBeneficiaryData.remove(atOffsets: offsets)
            for item in viewModel.getBeneficiaryData {
                self.dependentId = item.id ?? Int()
            }
        }
    }
    
    var body: some View {
        NavigationView {
//            ScrollView(.vertical, showsIndicators: false) {
                List {
                    ForEach(viewModel.getBeneficiaryData, id: \.self) { item in
                        DependentWithImageItem(dependant: item)
                    }
                    .onDelete(perform:
                                self.delete(at:)
                    )
                }
//            }//: ScrollView
                .navigationBarTitle("Your Dependents").font(.custom("Nunito-SemiBold", size: 20)).foregroundColor(Color("textColor"))
                .navigationBarBackButtonHidden(true)
                .toolbar {
                    ToolbarItem(placement: .navigationBarTrailing) {
                        NavigationLink(destination: AddNewDependantView(), isActive:  (self.$canAddDependants)) {
                            Button(action: {
                                if viewModel.getBeneficiaryData.count >= 5 {
                                    shouldShowLoginAlert = true
                                } else {
                                    canAddDependants = true
                                }
                                hapticImpact.impactOccurred()
                            }) {
                                Image(systemName: "person.fill.badge.plus")
                            }
                        }
                        .padding()
                        .alert(isPresented: $shouldShowLoginAlert) {
                            Alert(title: Text("OOPS!!!"), message: Text("It seems you already have 5 Dependants"))
                        }
                    }
                }
        }//NavigationView
//        .accentColor(Color("rov-blue"))
        .navigationBarHidden(true)
    }
}

struct DependantsView_Previews: PreviewProvider {
    static var previews: some View {
        DependantsView()
    }
}
