//
//  FeedbackView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/2/21.
//

import SwiftUI

struct FeedbackView: View {
    var body: some View {
        NavigationView {
            ScrollView {
                VStack {
                    ChatBubble(direction: .left) {
                        Text("Hello!")
                            .padding(.all, 20)
                            .foregroundColor(Color("textColor"))
                            .background(Color("chat-left"))
                    }
                    ChatBubble(direction: .right) {
                        Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ut semper quam. Phasellus non mauris sem. Donec sed fermentum eros. Donec pretium nec turpis a semper. ")
                            .padding(.all, 20)
                            .foregroundColor(Color("textColor"))
                            .background(Color("chat-right"))
                    }
                    ChatBubble(direction: .right) {
                        Image.init("test-image")
                            .resizable()
                            .frame(width: UIScreen.main.bounds.width - 70,
                                   height: 200).aspectRatio(contentMode: .fill)
                    }
                }
            }
            .navigationBarTitle("Customer Service")
            
            .navigationBarBackButtonHidden(true)
        }
        .navigationBarHidden(true)
    }
    
}

struct FeedbackView_Previews: PreviewProvider {
    static var previews: some View {
        FeedbackView()
    }
}
