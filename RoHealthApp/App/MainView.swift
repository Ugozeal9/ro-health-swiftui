//
//  MainView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/2/21.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        NavigationView {
            TabView {
                PlanView()
                    .tabItem {
                        Image(systemName: "briefcase.fill")
                        Text("Plan")
                    }
                DependantsView()
                    .tabItem {
                        Image(systemName: "person.3.fill")
                        Text("Dependants")
                    }
                FeedbackView()
                    .tabItem {
                        Image(systemName: "message.fill")
                        Text("Feedback")
                    }
                AccountView()
                    .tabItem {
                        Image(systemName: "person.fill")
                        Text("Account")
                    }
            }//: TabView
        }
        .accentColor(Color("rov-blue"))
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct MainView_Previews: PreviewProvider {
    static let plans: [PlanModel] = Bundle.main.decode("plans.json")
    static var previews: some View {
        MainView()
    }
}
