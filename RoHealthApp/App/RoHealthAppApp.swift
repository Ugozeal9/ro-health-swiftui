//
//  RoHealthAppApp.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/20/21.
//

import SwiftUI

@main
struct RoHealthAppApp: App {
    @AppStorage("isLoggedIn") var isLoggedIn: Bool = false
    var body: some Scene {
        WindowGroup {
            if isLoggedIn {
                LoginView()
            } else {
                ContentView()
            }
//            SignUpView()
//            RegisterDependantView()
//            DependantListView()
//            SelectHMOView()
//            PlanDetailsView()
        }
    }
}
