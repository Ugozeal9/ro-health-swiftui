//
//  NetworkClass.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/16/21.
//

import Foundation

final class NetworkClass {
    static let shared = NetworkClass()
}

extension NetworkClass {
    func verifyPhoneNumber(requestModel: PhoneNumberVerificationReq, success: @escaping (PhoneNumberVerificationResp) -> (), failure: @escaping (String) -> ()) {
        ApiClientWithoutHeaders.shared.execute(requestType: .post, url: K.Url.VERIFY_PHONENUMBER, params: ["mobile": requestModel.mobile ?? String()]) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func verifyOTP(requestModel: OTPVerificationReq, success: @escaping (OTPValidationResp) -> (), failure: @escaping (String) -> ()) {
        let refID = UserDefaults.standard.string(forKey: "refID")
        ApiClientWithoutHeaders.shared.execute(requestType: .post, url: "\(K.Url.VERIFY_OTP)?ref_id=\(refID ?? String())", params: ["otp": requestModel.otp ?? String()]) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func signUp(requestModel: StaffRegistrationReq, success: @escaping (ResistrationResp) -> (), failure: @escaping (String) -> ()) {
        ApiClientWithoutHeaders.shared.execute(requestType: .post, url: K.Url.SIGN_UP, params: ["address": requestModel.address ?? String(), "email": requestModel.email ?? String(), "first_name": requestModel.firstName ?? String(), "pin": requestModel.pin ?? String(), "gender": requestModel.gender ?? String(), "marital_status": requestModel.maritalStatus ?? String(), "state": requestModel.state ?? String(), "country": requestModel.country ?? String(), "last_name": requestModel.lastName ?? String(), "prev_condition": requestModel.prevCondition ?? String(), "mobile": requestModel.mobile ?? String()]) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func loginUser(requestModel: StaffLoginReq, success: @escaping (LoginResp) -> (), failure: @escaping (String) -> ()) {
        ApiClientWithoutHeaders.shared.execute(requestType: .post, url: K.Url.LOG_IN, params: ["email": requestModel.email ?? String(), "pin": requestModel.password ?? String()]) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func startPasswordReset(requestModel: PhoneNumberVerificationReq, success: @escaping (PhoneNumberVerificationResp) -> (), failure: @escaping (String) -> ()) {
        ApiClientWithoutHeaders.shared.execute(requestType: .post, url: K.Url.FORGOT_PASSWORD, params: ["mobile": requestModel.mobile ?? String()]) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func resetPassword(requestModel: SetNewPasswordReq, success: @escaping (ConfirmPasswordResp) -> (), failure: @escaping (String) -> ()) {
        let refID = UserDefaults.standard.string(forKey: "resetID")
        ApiClientWithoutHeaders.shared.execute(requestType: .post, url: "\(K.Url.RESET_PASSWORD)\(refID ?? "")", params: ["pin": requestModel.pin ?? String(), "cpin": requestModel.cpin ?? String()]) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func verifyResetPasswordOTP(requestModel: OTPVerificationReq, success: @escaping (OTPValidationResp) -> (), failure: @escaping (String) -> ()) {
        let refID = UserDefaults.standard.string(forKey: "resetID")
        ApiClientWithoutHeaders.shared.execute(requestType: .post, url: "\(K.Url.VERIFY_OTP)?ref_id=\(refID ?? "")", params: ["otp": requestModel.otp ?? String()]) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func addDependant(requestModel: AddDependantsReq, success: @escaping (GenericResponse) -> (), failure: @escaping (String) -> ()) {
        let token = UserDefaults.standard.string(forKey: "token")
        ApiClientWithHeaders.shared.execute(requestType: .post, headers: ["Authorization": "Bearer \(token ?? String())"], url: K.Url.ADD_BENEFICIARY, params: ["first_name": requestModel.firstName ?? String(), "last_name": requestModel.lastName ?? String(), "type": requestModel.type ?? String(), "mobile": requestModel.mobile ?? String()]) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func confirmBeneficiaries(success: @escaping (GenericResponse) -> (),  failure: @escaping (String) -> ()) {
        let token = UserDefaults.standard.string(forKey: "token")
        ApiClientWithHeaders.shared.purchase(headers: ["Authorization": "Bearer \(token ?? String())"], url: K.Url.CONFIRM_BENEFICIARIES) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func getNewBeneficiary(success: @escaping (GetBeneficiariesResp) -> (),  failure: @escaping (String) -> ()) {
        let token = UserDefaults.standard.string(forKey: "token")
        ApiClientWithHeaders.shared.loadData(requestType: .get, headers:  ["Authorization": "Bearer \(token ?? String())"], url: K.Url.GET_NEW_BENEFICIARIES) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func fetchBeneficiaries(success: @escaping (GetBeneficiariesResp) -> (),  failure: @escaping (String) -> ()) {
        let token = UserDefaults.standard.string(forKey: "token")
        ApiClientWithHeaders.shared.loadData(requestType: .get, headers:  ["Authorization": "Bearer \(token ?? String())"], url: K.Url.GET_BENEFICIARIES) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func deleteBeneficiary(id: Int, success: @escaping (GenericResponse) -> (),  failure: @escaping (String) -> ()) {
        let token = UserDefaults.standard.string(forKey: "token")
        ApiClientWithHeaders.shared.delete(headers: ["Authorization": "Bearer \(token ?? String())"], url: "\(K.Url.DELETE_BENEFICIARY)\(id)/delete") { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }

        
    }
    
    func fetchPackages(success: @escaping (GetPackagesResp) -> (),  failure: @escaping (String) -> ()) {
        let token = UserDefaults.standard.string(forKey: "token")
        ApiClientWithHeaders.shared.loadData(requestType: .get, headers:  ["Authorization": "Bearer \(token ?? String())"], url: K.Url.GET_PACKAGES) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func getPackage(success: @escaping (GetPackagesResp) -> (),  failure: @escaping (String) -> ()) {
        
    }
    
    func purchasePackage(id: Int, success: @escaping (ConfirmPurchaseResp) -> (),  failure: @escaping (String) -> ()) {
        let token = UserDefaults.standard.string(forKey: "token")
        ApiClientWithHeaders.shared.purchase(headers: ["Authorization": "Bearer \(token ?? String())"], url: "\(K.Url.PURCHASE_PACKAGE)\(id)") { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func getUserDetails(success: @escaping (GetUserDetailsResp) -> (),  failure: @escaping (String) -> ()) {
        let token = UserDefaults.standard.string(forKey: "token")
        ApiClientWithHeaders.shared.loadData(headers: ["Authorization": "Bearer \(token ?? String())"], url: K.Url.GET_USER_DETAILS) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func logoutUser(success: @escaping (GenericResponse) -> (),  failure: @escaping (String) -> ()) {
        ApiClientWithoutHeaders.shared.logOut(url: K.Url.LOG_OUT) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func loadHospitals(success: @escaping (GetAllHospitalsResp) -> (),  failure: @escaping (String) -> ()) {
        let token = UserDefaults.standard.string(forKey: "token")
        ApiClientWithHeaders.shared.loadData(headers: ["Authorization": "Bearer \(token ?? String())"], url: K.Url.GET_HOSPITALS) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func getTransactions(success: @escaping (GetTransactionsResp) -> (),  failure: @escaping (String) -> ()) {
        let token = UserDefaults.standard.string(forKey: "token")
        ApiClientWithHeaders.shared.loadData(headers: ["Authorization": "Bearer \(token ?? String())"], url: K.Url.GET_TRANSACTIONS) { (successMessage) in
            success(successMessage)
        } failure: { (error) in
            failure(error)
        }
    }
}
