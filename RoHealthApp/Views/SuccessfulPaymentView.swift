//
//  SuccessfulPaymentView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/3/21.
//

import SwiftUI

struct SuccessfulPaymentView: View {
    let plan: GetPackagesDetails //PlanModel
    var body: some View {
        VStack {
            ScrollView(.vertical, showsIndicators: false) {
                Section {
                    Image("congrats-image")
                        .resizable()
                        .scaledToFill()
                        .frame(width: 193, height: 173, alignment: .center)
                }
                .padding(EdgeInsets(top: 30, leading: 0, bottom: 50, trailing: 0))
               
            
                Section {
                    Text("Thank you for your purchase. Your package will be activated in \(Text("10 working days.").foregroundColor(Color("bronze-color"))) ")
                        .font(.custom("Nunito-Regular", size: 20))
                        .foregroundColor(Color("textColor"))
                        .multilineTextAlignment(.center)
                }
                .padding()
                Divider()
                    .padding()
                VStack(alignment: .center, spacing: 10) {
                    HStack {
                        Text("Plan:")
                            .font(.custom("Nunito-SemiBold", size: 14))
                            .foregroundColor(Color(.systemGray2))
                        Spacer()
                        Text("\(plan.name ?? String()) Plan")
                            .font(.custom("Nunito-SemiBold", size: 14))
                            .foregroundColor(Color("textColor"))
                    }
                    .padding()
                    
                    HStack {
                        Text("Purchase Date:")
                            .font(.custom("Nunito-SemiBold", size: 14))
                            .foregroundColor(Color(.systemGray2))
                        Spacer()
                        Text("11:30PM, 05/06/2020")
                            .font(.custom("Nunito-SemiBold", size: 14))
                            .foregroundColor(Color("textColor"))
                    }
                    .padding()
                    
                    HStack {
                        Text("Amount Paid:")
                            .font(.custom("Nunito-SemiBold", size: 14))
                            .foregroundColor(Color(.systemGray2))
                        Spacer()
                        Text(plan.price ?? String())
                            .font(.custom("Nunito-SemiBold", size: 14))
                            .foregroundColor(Color("textColor"))
                    }
                    .padding()
                }
                .padding(.vertical)
                Section {
                    NavigationLink(destination: PlanView()) {
                        HStack {
                         Text("Continue To Homepage")
                            .font(.custom("Nunito-SemiBold", size: 12))
                            .foregroundColor(Color("rov-blue"))
                            .padding()
                        }
                        .padding(.vertical, 10)
                        .background(Color("budget-Color"))
                        .cornerRadius(10)
                        .padding()
                    }
                }
            }
        }
        .navigationBarTitle("Your Payment is Successful", displayMode: .inline)
        .navigationBarBackButtonHidden(true)
    }
}

struct SuccessfulPaymentView_Previews: PreviewProvider {
    static let plans: [PlanModel] = Bundle.main.decode("plans.json")
    static var packages: [GetPackagesDetails] = ViewModel().getPackagesData
    static var previews: some View {
        SuccessfulPaymentView(plan: packages[0])
    }
}
