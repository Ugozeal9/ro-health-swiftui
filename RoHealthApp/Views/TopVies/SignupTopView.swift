//
//  SignupTopView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/23/21.
//

import SwiftUI

struct SignupTopView: View {
    let backgroundImage: String = "onboarding1-image"
    let titleText: String = "Kindly fill your profile so you can start enjoying Insurance cover"
    var body: some View {
        ZStack {
            Color("topView-color")
                .ignoresSafeArea()
            Image(backgroundImage)
                .resizable()
                .frame(width:250, height: 200)
                .background(
                    Color("topView-color")
                )
                .padding(EdgeInsets(top: -50, leading: 0, bottom: 0, trailing: 0))
            Text(titleText)
                .multilineTextAlignment(.center)
                .lineLimit(2)
                .foregroundColor(.white)
                .font(.custom("Nunito-SemiBold", size: 17))
                .padding(EdgeInsets(top: 0, leading: 0, bottom: -50, trailing: 0))
//                .padding(.vertical, 50)
        }//:ZStack
    }
}

struct SignupTopView_Previews: PreviewProvider {
    static var previews: some View {
        SignupTopView()
            .previewLayout(.sizeThatFits)
    }
}
