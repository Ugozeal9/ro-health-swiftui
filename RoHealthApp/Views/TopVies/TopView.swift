//
//  TopView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/20/21.
//

import SwiftUI

struct TopView: View {
    //MARK: PROPERTIES
    
    //MARK: BODY
    var body: some View {
        ZStack {
            Image("login-topView-image")
                .resizable()
                .scaledToFill()
                .background(
                    Color("topView-color")
                )
            Image("logo-image")
                .resizable()
                .frame(width: 171, height: 50, alignment: .center)
        }
    }
}

//MARK: PREVIEW
struct TopView_Previews: PreviewProvider {
    static var previews: some View {
        TopView()
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
