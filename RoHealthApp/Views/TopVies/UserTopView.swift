//
//  UserTopView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/24/21.
//

import SwiftUI

struct UserTopView: View {
    var regData: RegistrationUserResp
    var body: some View {
        HStack(alignment: .center, spacing: 16)  {
            Image("test-image")
                .resizable()
                .scaledToFill()
                .frame(width:50, height: 50)
                .clipShape(
                    Circle()
                )
            VStack(alignment: .leading, spacing: 8) {
                Text("Hello, \n\(regData.firstName ?? String())")
                    .font(.custom("Nunito-SemiBold", size: 19))
                    .foregroundColor(Color("textColor"))
            }
            Spacer()
            Button(action: {
                print("Something")
            }) {
                Image(systemName: "bell.badge.fill")
                    .resizable()
                    .frame(width: 22, height: 22)
                    .foregroundColor(Color("rov-blue"))
            }
        }
        .padding()
        .frame(minHeight: 0, maxHeight: 100)
        .frame(minWidth: 0, maxWidth: 400)
    }
}

struct UserTopView_Previews: PreviewProvider {
    static var userData: RegistrationUserResp = ViewModel().signupResp
    static var previews: some View {
        UserTopView(regData: userData)
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
