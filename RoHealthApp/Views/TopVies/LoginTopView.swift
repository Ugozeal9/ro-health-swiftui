//
//  LoginTopView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 4/26/21.
//

import SwiftUI

struct LoginTopView: View {
    var loginData: UserDetailsEntity
    var body: some View {
        HStack(alignment: .center, spacing: 16)  {
            Image("test-image")
                .resizable()
                .scaledToFill()
                .frame(width:50, height: 50)
                .clipShape(
                    Circle()
                )
            VStack(alignment: .leading, spacing: 8) {
                Text("Hello, \n\(loginData.firstName ?? String())")
                    .font(.custom("Nunito-SemiBold", size: 19))
                    .foregroundColor(Color("textColor"))
            }
            Spacer()
            Button(action: {
                print("Something")
            }) {
                Image(systemName: "bell.badge.fill")
                    .resizable()
                    .frame(width: 22, height: 22)
                    .foregroundColor(Color("rov-blue"))
            }
        }
        .padding()
        .frame(minHeight: 0, maxHeight: 100)
        .frame(minWidth: 0, maxWidth: 400)
    }
}

struct LoginTopView_Previews: PreviewProvider {
    static var userData: UserDetailsEntity = ViewModel().getUserDetails
    static var previews: some View {
        LoginTopView(loginData: userData)
    }
}
