//
//  DependantsTopView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/24/21.
//

import SwiftUI

struct DependantsTopView: View {
    var title: String
    var body: some View {
        ZStack {
            Image("dependants-cover")
                .resizable()
                .background(
                    Color("topView-color")
                )
            Text(title)
                .multilineTextAlignment(.center)
                .lineLimit(2)
                .foregroundColor(.white)
                .font(.custom("Nunito-Bold", size: 17))
                .frame(height: 50, alignment: .center)
                .padding()
    }    }
}

struct DependantsTopView_Previews: PreviewProvider {
    static var previews: some View {
        DependantsTopView(title: "Add up benefeciaries you would like to also enjoy Insurance cover")
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
