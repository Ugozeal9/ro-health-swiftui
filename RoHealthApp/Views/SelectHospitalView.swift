//
//  SelectHospitalView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/26/21.
//

import SwiftUI

struct SelectHospitalView: View {
    @State var searchText = ""
    @State var isSearching = false
    @StateObject var viewModel = ViewModel()
    
    var body: some View {
        VStack {
            ScrollView(.vertical, showsIndicators: false) {
                SearchBar(searchText: $searchText, isSearching: $isSearching)
                    .padding(EdgeInsets(top: 20, leading: 0, bottom: 20, trailing: 0))
                ForEach(viewModel.getHospitalData.filter({"\($0)".contains(searchText) || searchText.isEmpty})) { item in
                    HospitalListItem(hospital: item)
                        Divider()
                    }
                    .padding(.horizontal)
            }
            .navigationBarTitle("Choose Preferred Hospital", displayMode: .inline).font(.custom("Nunito-SemiBold", size: 16))
        }
    }
}

struct SelectHospitalView_Previews: PreviewProvider {
    static var previews: some View {
        SelectHospitalView()
    }
}
