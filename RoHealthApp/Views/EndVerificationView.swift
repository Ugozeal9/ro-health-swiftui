//
//  EndVerificationView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/12/21.
//

import SwiftUI

struct EndVerificationView: View {
    @State var OTP: String = ""
    @State var isOTPEntered: Bool = false
    @State private var shouldShowLoginAlert: Bool = false
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @ObservedObject var viewModel = ViewModel()
    
    var body: some View {
        VStack {
            TopView()
                .frame(height: 170)
                .edgesIgnoringSafeArea(.all)
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            ScrollView {
                Section {
                    Text("Enter The OTP You Received")
                        .font(.custom(K.Fonts.semiBold, fixedSize: 18))
                        .foregroundColor(.black)
                }
                
                Section {
                    HStack {
                        TextField("Enter six digits OTP ... ", text: $OTP)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                
                Section {
                    NavigationLink(destination: SignUpView(), isActive:  (self.$isOTPEntered)) {
                        Button(action: {
                            let notRegistered = self.OTP.count
                            //trigger logic
                            if notRegistered == 6 {
                                viewModel.verifyOTP(otp: OTP)
                                if viewModel.otpCode == 100 {
                                    self.isOTPEntered = true //trigger NavigationLink
                                } else {
                                    self.shouldShowLoginAlert = true //trigger Alert
                                }
                            } else {
                                self.shouldShowLoginAlert = true //trigger Alert
                            }
                        }, label: {
                            DefaultButtonView(buttonText: "Submit", textColor: .white, backgroundColor: Color("rov-blue"), borderColor: Color("rov-blue"))
                        })

                    } .alert(isPresented: $shouldShowLoginAlert) {
                        Alert(title: Text("OOPS!!!"), message: Text(viewModel.otpMessage))
                    }
                }
                .disabled(OTP.isEmpty)
                Section {
                    HStack {
                        Button(action:  {
                            self.mode.wrappedValue.dismiss()
                        }) {
                            DefaultButtonView(buttonText: "<< Go Back", textColor: .gray, backgroundColor: .clear, borderColor: .clear)
                        }
                    }
                }
            }
        }
        .onTapGesture {hideKeyboard()}
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct EndVerificationView_Previews: PreviewProvider {
    static var previews: some View {
        EndVerificationView()
    }
}
