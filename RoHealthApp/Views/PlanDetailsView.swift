//
//  PlanDetailsView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/25/21.
//

import SwiftUI

struct PlanDetailsView: View {
    //MARK:- Properties
    @State var searchText = ""
    @State var isSearching = false
    @State var hasSelectedHospital: Bool = false
    @State var hasSelectedPlan: Bool = false
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @StateObject var viewModel = ViewModel()
    @State private var shouldShowLoginAlert: Bool = false
    
//    let plan: PlanModel
    @State var package: GetPackagesDetails
    
    func loadPackage() {
        print(package.id ?? Int(), "???")
    }
    var body: some View {
        VStack {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center, spacing: 20) {
                    HStack {
                        Text("Plan Covers: ")
                            .underline()
                            .foregroundColor(.accentColor)
                            .font(.custom("Nunito-SemiBold", size: 14))
                        Spacer()
                        NavigationLink(destination: AllPlanDetailsView(plan: package)) {
                            Text("View All")
                                .foregroundColor(Color("rov-blue"))
                                .font(.custom("Nunito-SemiBold", size: 11))
                        }
                    }
                    .padding()
                    PlanCoverItem(plan: package)
                    Section {
                        NavigationLink(destination: SelectHospitalView(), isActive: $hasSelectedHospital) {
                            HStack {
                                Text("Search Hospitals That You Can use This Plan At:")
                                    .underline()
                                    .foregroundColor(.accentColor)
                                    .font(.custom("Nunito-SemiBold", size: 14))
                                Spacer()
                            }
                            .padding()
                        }
                    }
                    
//                    SearchBar(searchText: $searchText, isSearching: $isSearching)
                    Section {
                        NavigationLink(destination: CheckOutView(plan: package), isActive: $hasSelectedPlan) {
                            Button(action: {
                                viewModel.purchasePackage(id: package.id ?? 0)
                                let delay = DispatchTime.now() + .milliseconds(700)
                                DispatchQueue.main.asyncAfter(deadline: delay) {
                                    if viewModel.purchasePackageCode == 100 {
                                        hasSelectedPlan = true
                                    } else {
                                        shouldShowLoginAlert = true
                                    }
                                }
                            }, label: {
                                HStack {
                                    Text("Select this Plan")
                                        .foregroundColor(.white)
                                        .font(.custom("Nunito-SemiBold", size: 12))
                                        .padding(.horizontal)
                                }
                                .frame(height: 30)
                                .padding(.vertical)
                                .background(Color("rov-blue"))
                                .cornerRadius(10)
                                .padding()
                            })
                        }.alert(isPresented: $shouldShowLoginAlert) {
                            Alert(title: Text("OOPS!!!"), message: Text(viewModel.purchasePackageMess))
                        }
                    }
                    Section {
                        HStack {
                            VStack {
                                Text("Add more money to purchase a higher plan")
                                    .foregroundColor(Color("textColor"))
                                    .font(.custom("Nunito-Regular", size: 15))
                                    .padding()
                                Button(action: {
                                    
                                }, label: {
                                    HStack {
                                        Text("See Plans and Coverage")
                                            .foregroundColor(Color("rov-blue"))
                                            .font(.custom("Nunito-Regular", size: 12))
                                    }
                                    .padding()
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 10)
                                            .stroke(Color("rov-blue"), lineWidth: 1)
                                    )
                                    .background(RoundedRectangle(cornerRadius: 10).fill(Color.clear))
                                })
                            }
                        }
                        .frame(height: 140)
                        .frame(minWidth: 0, maxWidth: 370)
                        .padding(.vertical)
                        .background(Color("budget-Color"))
                        .cornerRadius(10)
                        .padding()
                    }
                    
                }//VSTACK
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            }//SCROLLVIEW
        }//VSTACK
        .navigationBarTitle("\(package.name ?? "") Package", displayMode: .inline)
        .onAppear {
            loadPackage()
        }
        
    }
}

struct PlanDetailsView_Previews: PreviewProvider {
//    static let plans: [PlanModel] = Bundle.main.decode("plans.json")
    static let packages: [GetPackagesDetails] = ViewModel().getPackagesData
    static var previews: some View {
        PlanDetailsView(package: packages[0])
            .previewDevice("iPhone 12 Pro")
    }
}
