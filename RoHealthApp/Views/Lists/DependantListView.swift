//
//  DependantListView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/24/21.
//

import SwiftUI

struct DependantListView: View {
    @State var hasAdded: Bool = false
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @State var isNavigationBarHidden: Bool = true
    @State private var shouldShowLoginAlert: Bool = false
    @StateObject var viewModel = ViewModel()
    
    var body: some View {
        VStack {
            DependantsTopView(title: "Dependent Summary List")
                .frame(height: 170)
                .edgesIgnoringSafeArea(.all)
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            List {
                
                ForEach(viewModel.getNewBenArray, id: \.self) { dependant in
                    SingleDependantListView(dependant: dependant)
                        .padding(.vertical, 8)
                }
            }
            .listStyle(InsetGroupedListStyle())
            .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            Spacer()
            
            //MARK: - NAVIGATION LINKS
            Section {
                NavigationLink(destination: SelectHMOView(), isActive: self.$hasAdded) {
                    Button(action: {
                        viewModel.confirmBeneficiaries()
                        let delay = DispatchTime.now() + .milliseconds(700)
                        DispatchQueue.main.asyncAfter(deadline: delay) {
                            if viewModel.confirmBenCode == 100 {
                                self.hasAdded = true //trigger NavigationLink
                            } else {
                                self.shouldShowLoginAlert = true //trigger Alert
                            }
                        }
                    }, label: {
                        DefaultButtonView(buttonText: "Confirm and Proceed", textColor: .white, backgroundColor: Color("rov-blue"), borderColor: Color("rov-blue"))
                    })
                }
                .alert(isPresented: $shouldShowLoginAlert) {
                    Alert(title: Text("OOPS!!!"), message: Text(viewModel.confirmBenMess))
                }
                
                Button(action:  {
                    self.mode.wrappedValue.dismiss()
                }) {
                    DefaultButtonView(buttonText: "<< Go back To Add More Dependent", textColor: .gray, backgroundColor: .clear, borderColor: .clear)
                }
            }
            
        }//VSTack
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .environmentObject(viewModel)
    }
}

struct DependantListView_Previews: PreviewProvider {
    static var previews: some View {
        DependantListView()
    }
}
