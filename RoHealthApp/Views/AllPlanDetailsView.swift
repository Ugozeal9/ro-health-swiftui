//
//  AllPlanDetailsView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/4/21.
//

import SwiftUI

struct AllPlanDetailsView: View {
    let plan: GetPackagesDetails //PlanModel

    var body: some View {
        VStack {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center, spacing: 20) {
                    HStack {
                        Text("Plan Covers: ")
                            .underline()
                            .foregroundColor(Color("bronze-color"))
                            .font(.custom("Nunito-SemiBold", size: 14))
                        Spacer()
                      
                    }
                    .padding()
                    AllPlanCoverItem(plan: plan)
                    
                    
                }//VSTACK
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            }//SCROLLVIEW
        }//VSTACK
        .navigationBarTitle(plan.name ?? "", displayMode: .inline)
    }
}

struct AllPlanDetailsView_Previews: PreviewProvider {
    static let plans: [PlanModel] = Bundle.main.decode("plans.json")
    static let packages: [GetPackagesDetails] = ViewModel().getPackagesData
    static var previews: some View {
        AllPlanDetailsView(plan: packages[0])
    }
}
