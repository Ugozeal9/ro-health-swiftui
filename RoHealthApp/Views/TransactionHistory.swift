//
//  TransactionHistory.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/3/21.
//

import SwiftUI

struct TransactionHistory: View {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @StateObject var viewModel = ViewModel()
    
    func customDateFormatter(date: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, yyyy"
        let date: Date? = dateFormatterGet.date(from: date)
        return dateFormatter.string(from: date!)
    }

    var body: some View {
        VStack {
            ScrollView(.vertical, showsIndicators: false) {
                ForEach(viewModel.getTransactionsData, id: \.self) { item in
                    TransactionsListItem(transaction: item)
                        .padding(.horizontal)
                        .padding(.vertical, 5)
                }
            }
            .navigationBarTitle("Transactions History", displayMode: .inline)//.font(.custom("Nunito-SemiBold", size: 16))
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.mode.wrappedValue.dismiss()
                                    }, label: {
                                        Image(systemName: "arrow.backward")
                                            .foregroundColor(Color("textColor"))
                                        
                                    })
            )
        }
    }
}

struct TransactionHistory_Previews: PreviewProvider {
    static var previews: some View {
        TransactionHistory()
    }
}
