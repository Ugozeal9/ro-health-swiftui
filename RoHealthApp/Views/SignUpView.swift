//
//  SignUpView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/20/21.
//

import SwiftUI

struct SignUpView: View {
    @State var firstname: String = ""
    @State var lastname: String = ""
    @State var emailAddress: String = ""
    @State var phoneNumber: String = ""
    @State var houseAddress: String = ""
    @State var password: String = ""
    @State var staffGender: String = ""
    @State var staffMaritalStatus: String = ""
    @State var staffCountry: String = ""
    @State var staffState: String = ""
    @State var selectedGender: Int?
    @State var selectedMaritalStatus: Int?
    @State var selectedCountry: Int?
    @State var selectedState: Int?
    @State var preExistingCondition: String = ""
    @State var isNotRegistered: Bool = false
    @State var skipRegistration: Bool = false
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @State var isPhoneNumberEntered: Bool = false
    @State private var shouldShowLoginAlert: Bool = false
    @ObservedObject var viewModel = ViewModel()
    
    func getPickerValue( selectedIndex: Int, in array: [String]) -> String? {
        for (index, value) in array.enumerated()
        {
            if index == selectedIndex {
                return value
            }
        }
        return nil
    }
    
    
    var body: some View {
        VStack {
            SignupTopView()
                .frame(height: 170)
                .edgesIgnoringSafeArea(.all)
                .padding(EdgeInsets(top: 0, leading: 0, bottom: -50, trailing: 0))
            Spacer()
            ScrollView(.vertical,showsIndicators: false) {
                Section {
                    HStack {
                        Image(systemName: "person")
                        TextField("First Name", text: $firstname)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                            .keyboardType(.numberPad)
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                Section {
                    HStack {
                        Image(systemName: "person")
                        TextField("Last Name", text: $lastname)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                            .keyboardType(.numberPad)
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                Section {
                    HStack {
                        Image(systemName: "envelope")
                        TextField("Email Address", text: $emailAddress)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                            .keyboardType(.emailAddress)
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                Section {
                    HStack {
                        Image(systemName: "phone")
                        TextField("Phone Number", text: $phoneNumber)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                            .keyboardType(.phonePad)
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                Section {
                    HStack {
                        HStack {
                            PickerTextField(lastSelectedIndex: $selectedGender, data: K.ArrayList.genderList, placeholder: "Select Gender")
                            
                        }
                        .frame(minWidth: 0, maxWidth: 370, minHeight: 0, maxHeight: 20)
                        .padding()
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                        .padding()
                        HStack {
                            PickerTextField(lastSelectedIndex: $selectedMaritalStatus, data: K.ArrayList.maritalStatusList, placeholder: "Marital Status")
                        }
                        .frame(minWidth: 0, maxWidth: 370)
                        .padding()
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                        .padding()
                    }
                    
                }
                Section {
                    HStack {
                        Image(systemName: "mappin.and.ellipse")
                        TextField("House Address", text: $houseAddress)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                            .keyboardType(.default)
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                Section {
                    HStack {
                        HStack {
                            PickerTextField(lastSelectedIndex: $selectedState, data: K.ArrayList.stateList, placeholder: "State")
                        }
                        .frame(minWidth: 0, maxWidth: 370)
                        .padding()
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                        .padding()
                        HStack {
                            PickerTextField(lastSelectedIndex: $selectedCountry, data: K.ArrayList.countryList, placeholder: "Country")
                        }
                        .frame(minWidth: 0, maxWidth: 370)
                        .padding()
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                        .padding()
                    }
                    
                }
                
                Section {
                    HStack {
                        TextField("Enter Pre-Existing Conditions If Any", text: $preExistingCondition)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                            .keyboardType(.default)
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                
                Section {
                    HStack {
                        SecureField("Set a 4 Digit Pin For Easy Access To Your App", text: $password)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                            .keyboardType(.numberPad)
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                
                //MARK: - NAVIGATION LINKS
                Section {
//                    NavigationLink(destination: RegisterDependantView(), isActive:  (self.$isNotRegistered)) {
                        Button(action: {
                            //                            print("HERE")
                            staffMaritalStatus = getPickerValue(selectedIndex: selectedMaritalStatus ?? 0, in: K.ArrayList.maritalStatusList) ?? String()
                            staffGender = getPickerValue(selectedIndex: selectedGender ?? 0, in: K.ArrayList.genderList) ?? String()
                            staffState = getPickerValue(selectedIndex: selectedState ?? 0, in: K.ArrayList.stateList) ?? String()
                            staffCountry = getPickerValue(selectedIndex: selectedCountry ?? 0, in: K.ArrayList.countryList) ?? String()
                            
                            let notRegistered = self.firstname.isEmpty == false && self.lastname.isEmpty == false && self.emailAddress.isEmpty == false && staffState.isEmpty == false && staffGender.isEmpty == false && staffCountry.isEmpty == false && staffMaritalStatus.isEmpty == false && password.isEmpty == false && houseAddress.isEmpty == false && preExistingCondition.isEmpty == false && phoneNumber.isEmpty == false
                            //trigger logic
                            if notRegistered {
                                viewModel.signupStaff(firstName: firstname, email: emailAddress, gender: staffGender, pin: password, address: houseAddress, country: staffCountry, maritalStatus: staffMaritalStatus, state: staffState, lastName: lastname, prevCondition: preExistingCondition, mobile: phoneNumber)
                                if viewModel.signupCode == 100 {
                                    self.isNotRegistered = true //trigger NavigationLink
                                } else {
                                    self.shouldShowLoginAlert = true
                                }
                            } else {
                                self.shouldShowLoginAlert = true //trigger Alert
                            }
                        }, label: {
                            DefaultButtonView(buttonText: "Save and Continue", textColor: .white, backgroundColor: Color("rov-blue"), borderColor: Color("rov-blue"))
                        })//.disabled(true)
                     .alert(isPresented: $shouldShowLoginAlert) {
                        Alert(title: Text("OOPS!!!"), message: Text(viewModel.signupMessage))
                    }
                    
                    Button(action:  {
                        self.mode.wrappedValue.dismiss()
                    }) {
                        DefaultButtonView(buttonText: "<< I'll Do This Later", textColor: .gray, backgroundColor: .clear, borderColor: .clear)
                    }
                }
            }
            .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            Spacer()
        }
        .navigationBarBackButtonHidden(true)
    }
}

struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpView()
    }
}
