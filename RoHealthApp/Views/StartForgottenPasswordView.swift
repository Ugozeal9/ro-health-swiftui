//
//  ForgottenPassword.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/24/21.
//

import SwiftUI

struct StartForgottenPasswordView: View {
    @State var phoneNumber: String = ""
    @State var isInputFieldValid: Bool = false
    @State private var shouldShowLoginAlert: Bool = false
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @ObservedObject var viewModel = ViewModel()

    var body: some View {
        VStack {
            TopView()
                .frame(height: 170)
                .edgesIgnoringSafeArea(.all)
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            ScrollView {
                Section {
                    Text("Forgot Password")
                        .font(.custom(K.Fonts.semiBold, fixedSize: 18))
                        .foregroundColor(.black)
                }
                
                Section {
                    HStack {
                        TextField("Enter Email or Phone Number", text: $phoneNumber)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
              
                Section {
                    NavigationLink(destination: EnterForgottenPasswordOTPView(), isActive:  (self.$isInputFieldValid)) {
                        Button(action: {
                            let inputIsValid = self.phoneNumber.count == 11
                            //trigger logic
                            if inputIsValid {
                                viewModel.forgotPassword(phoneNumber: phoneNumber)
                                self.isInputFieldValid = true
                            } else {
                                self.shouldShowLoginAlert = true //trigger Alert
                            }
                        }, label: {
                            DefaultButtonView(buttonText: "Continue", textColor: .white, backgroundColor: Color("rov-blue"), borderColor: Color("rov-blue"))
                        })

                    } .alert(isPresented: $shouldShowLoginAlert) {
                        Alert(title: Text("OOPS!!!"), message: Text(viewModel.forgotPasswordMess))
                    }
                }
                .disabled(phoneNumber.count != 11)
                Section {
                    HStack {
                        Text("Remember Your Password?")
                        NavigationLink(
                            destination: LoginView(),
                            label: {
                                Text("Login Here")
                                    .underline()
                        })
                    }
                    .foregroundColor(.black)
                    .font(Font.custom("Nunito-SemiBold", size: 13))
                    .padding()
                }
            }
            Spacer()
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct ForgottenPassword_Previews: PreviewProvider {
    static var previews: some View {
        StartForgottenPasswordView()
    }
}
