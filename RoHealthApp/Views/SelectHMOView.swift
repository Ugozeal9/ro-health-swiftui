//
//  SelectHMOView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/24/21.
//

import SwiftUI

struct SelectHMOView: View {
    //MARK:- Properties
//    let plans: [PlanModel] = Bundle.main.decode("plans.json")
    var packages: [GetPackagesDetails] = []
    @State var searchText = ""
    @State var isSearching = false
    @StateObject var viewModel = ViewModel()
    var budget = ""
    
    func loadPackages() {
        viewModel.getPackages()
        print(viewModel.getPackagesData, "|||")
    }
    var body: some View {
        ZStack {
            VStack(spacing: 0) {
                UserTopView(regData: viewModel.signupResp)
                    .padding(.bottom)
                    .padding(.top, UIApplication.shared.windows.first?.safeAreaInsets.top)
                ScrollView(.vertical, showsIndicators: false) {
                    SearchBar(searchText: $searchText, isSearching: $isSearching)
                    HStack {
                        HStack {
                            VStack(alignment: .leading) {
                                Text("Budget:")
                                    .foregroundColor(Color("textColor"))
                                    .font(.custom("Nunito-Regular", size: 13))
                                Text("₦ \(viewModel.budget)")
                                    .foregroundColor(Color("rov-blue"))
                                    .font(.custom("Nunito-Bold", size: 24))
                            }//VSTACK
                            .padding(.leading)
                        }//HSTACK
                        Spacer()
                        HStack {
                            Image("budget-image")
                        }
                        .padding(.trailing)
                    }//HSTACK
                    .frame(minHeight: 0, maxHeight: 100)
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding(.vertical)
                    .background(Color("budget-Color"))
                    .cornerRadius(10)
                    .padding()
                    Section {
                        HStack {
                            Text("Available HMO packages:")
                                .foregroundColor(Color("textColor"))
                                .font(.custom("Nunito-SemiBold", size: 15))
                            Spacer()
                        }
                        .padding(.leading)
                    }
                    Section {
                        ForEach (viewModel.getPackagesData.filter({"\($0)".contains(searchText) || searchText.isEmpty})) { plan in
                            NavigationLink(destination: PlanDetailsView(package: plan)) {
                                PlanListItem(package: plan)
                            }//.padding([.trailing], -30.0)
                            //.padding([.leading], -10)
                        }
                        .padding()
                    }
                    
                    Spacer()
                }
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
                
                
            }//VSTACK
            
            .navigationBarTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            .environmentObject(viewModel)
            .onAppear {
                loadPackages()
            }
        }//ZSTACK
        .ignoresSafeArea(.all, edges: .all)
    }
    
}

struct SelectHMOView_Previews: PreviewProvider {
    static var previews: some View {
        SelectHMOView()
    }
}
