//
//  LoginView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/20/21.
//

import SwiftUI

struct LoginView: View {
    @State var notRegistered = false
    @State var emailOrPhone: String = ""
    @State var password: String = ""
    @State private var shouldShowLoginAlert: Bool = false
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @StateObject var viewModel = ViewModel()
    @AppStorage("isLoggedIn") var isLoggedIn: Bool?
    
    func errorText() -> String {
        if emailOrPhone == "" || password == ""{
            return "One or more input fields are empty"
        } else {
            return viewModel.loginMessage
        }
    }
  
    var body: some View {
        NavigationView {
            VStack(spacing: 5) {
                TopView()
                    .frame(height: 170)
                    .edgesIgnoringSafeArea(.all)
                    .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
                ScrollView {
                    Section {
                        Text("Login to your RoHealth Account")
                            .font(.custom(K.Fonts.semiBold, fixedSize: 18))
                            .foregroundColor(.black)
                    }
                    Section {
                        HStack {
                            Image(systemName: "briefcase.fill")
                            TextField("Enter your Email or Phone Number", text: $emailOrPhone)
                                .font(Font.custom("Nunito-SemiBold", size: 12))
                        }
                        .frame(minWidth: 0, maxWidth: 370)
                        .padding()
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                        .padding()
                    }
                    
                    Section {
                        HStack {
                            SecureField("Enter your 4 digit pin", text: $password)
                                .font(Font.custom("Nunito-SemiBold", size: 12))
                                .keyboardType(.numberPad)
                        }
                        .frame(minWidth: 0, maxWidth: 370)
                        .padding()
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                        .padding()
                    }
                    Section {
                        HStack {
                            Spacer()
                            NavigationLink(
                                destination: StartForgottenPasswordView(),
                                label: {
                                    Text("Forgot Password?")
                                        .font(Font.custom("Nunito-SemiBold", size: 12))
                            })
                        }
                        .padding()
                    }
                    Section {
                        NavigationLink(destination: MainView(), isActive:  (self.$notRegistered)) {
                            Button(action: {
                                let notRegistered = self.emailOrPhone.isEmpty == false && self.password.isEmpty == false
                                //trigger logic
                                if notRegistered {
                                    viewModel.loginStaff(email: emailOrPhone, password: password)
                                    let delay = DispatchTime.now() + .milliseconds(700)
                                    DispatchQueue.main.asyncAfter(deadline: delay) {
                                        if viewModel.loginCode == 100 {
                                            self.notRegistered = true //trigger NavigationLink
                                            isLoggedIn = true
                                        } else {
                                            self.shouldShowLoginAlert = true //trigger Alert
                                        }
                                    }
                                } else {
                                    self.shouldShowLoginAlert = true //trigger Alert
                                }
                            }, label: {
                                DefaultButtonView(buttonText: "Login To My Account", textColor: .white, backgroundColor: Color("rov-blue"), borderColor: Color("rov-blue"))
                            })

                        } .alert(isPresented: $shouldShowLoginAlert) {
                            Alert(title: Text("OOPS!!!"), message: Text(errorText()))
                        }
                    }
                    .disabled(emailOrPhone.isEmpty || password.isEmpty)
                    Section {
                        if isLoggedIn == true {
                            
                        } else {
                            Button(action:  {
                                self.mode.wrappedValue.dismiss()
                            }) {
                                DefaultButtonView(buttonText: "<< Go Back", textColor: .gray, backgroundColor: .clear, borderColor: .clear)
                            }
                        }
                    }
                }
                Spacer()
            }
            .navigationBarTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            .environmentObject(viewModel)
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
