//
//  StartVerificationView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/12/21.
//

import SwiftUI
import UIKit
#if canImport(UIKit)
extension View {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
#endif

struct StartVerificationView: View {
    @State var phoneNumber: String = ""
    @State var isPhoneNumberEntered: Bool = false
    @State private var shouldShowLoginAlert: Bool = false
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @ObservedObject var viewModel = ViewModel()

    var body: some View {
        VStack {
            TopView()
                .frame(height: 170)
                .edgesIgnoringSafeArea(.all)
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            ScrollView {
                Section {
                    Text("Welcome to RoHealth App")
                        .font(.custom(K.Fonts.semiBold, fixedSize: 18))
                        .foregroundColor(.black)
                }
                
                Section {
                    HStack {
                        TextField("Enter your Phone Number", text: $phoneNumber)
                            .keyboardType(.numberPad)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                
                Section {
                    NavigationLink(destination: EndVerificationView(), isActive:  (self.$isPhoneNumberEntered)) {
                        Button(action: {
                            let notRegistered = self.phoneNumber.count
                            //trigger logic
                            if notRegistered == 11 {
                                viewModel.verifyPhoneNumber(phoneNumber: phoneNumber)
                                if viewModel.verificationCode == 100 {
                                    self.isPhoneNumberEntered = true //trigger NavigationLink
                                } else {
                                    self.shouldShowLoginAlert = true
                                }
                            } else {
                                self.shouldShowLoginAlert = true //trigger Alert
                            }
                        }, label: {
                            DefaultButtonView(buttonText: "Enter Your Account", textColor: .white, backgroundColor: Color("rov-blue"), borderColor: Color("rov-blue"))
                        })

                    } .alert(isPresented: $shouldShowLoginAlert) {
                        Alert(title: Text("OOPS!!!"), message: Text(viewModel.verificationMessage))
                    }
                }
                .disabled(phoneNumber.isEmpty)
                
                Section {
                    HStack {
                        Button(action:  {
                            self.mode.wrappedValue.dismiss()
                        }) {
                            DefaultButtonView(buttonText: "<< Go Back", textColor: .gray, backgroundColor: .clear, borderColor: .clear)
                        }
                    }
                }
            }
        }
        .onTapGesture {hideKeyboard()}
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct StartVerificationView_Previews: PreviewProvider {
    static var previews: some View {
        StartVerificationView()
    }
}
