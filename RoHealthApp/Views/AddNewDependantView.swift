//
//  AddNewDependantView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 4/27/21.
//

import SwiftUI

struct AddNewDependantView: View {
    @State var numOfRegDependants: Int = ViewModel().getNewBenArray.count
    @State var selectedDependantType: Int?
    @State var firstname: String = ""
    @State var lastname: String = ""
    @State var phoneNumber: String = ""
    @State var dependantType: String = ""
    @State var isDoneReg: Bool = false
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @State var isInputFieldValid: Bool = false
    @State private var shouldShowLoginAlert: Bool = false
    @StateObject var viewModel = ViewModel()
    func getPickerValue( selectedIndex: Int, in array: [String]) -> String? {
        for (index, value) in array.enumerated()
        {
            if index == selectedIndex {
                return value
            }
        }
        return nil
    }
    
    func clearInputView() {
        dependantType = ""
        phoneNumber = ""
        firstname = ""
        lastname = ""
    }
    
    func errorText() -> String {
        if lastname == "" || phoneNumber == "" || firstname == "" || dependantType == "" {
            return "One or more input fields are empty"
        } else {
            return viewModel.addBeneficiaryMess
        }
    }
    
    func fetchDependantData() {
        numOfRegDependants+=1
    }
    
    func showNumber() {
            let delay = DispatchTime.now() + .milliseconds(1000)
            DispatchQueue.main.asyncAfter(deadline: delay) {
                numOfRegDependants = viewModel.getBeneficiaryData.count
        }
    }
    var body: some View {
        VStack {
            DependantsTopView(title: "Add up benefeciaries you would like to also enjoy Insurance cover")
                .frame(height: 170)
                .edgesIgnoringSafeArea(.all)
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            ScrollView(.vertical,showsIndicators: false) {
                Section {
                    Text("\(numOfRegDependants)/5 Dependents Added")
                        .foregroundColor(.accentColor)
                        .font(.custom("Nunito-SemiBold", size: 13))
                }
                Section {
                    HStack {
                        PickerTextField(lastSelectedIndex: $selectedDependantType, data: K.ArrayList.dependantList, placeholder: "Select Dependant Type")
                    }
                    .frame(minWidth: 0, maxWidth: 370, minHeight: 0, maxHeight: 20)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                
                Section {
                    HStack {
                        Image(systemName: "person")
                        TextField("First Name", text: $firstname)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                            .keyboardType(.numberPad)
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                Section {
                    HStack {
                        Image(systemName: "person")
                        TextField("Last Name", text: $lastname)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                            .keyboardType(.numberPad)
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                
                Section {
                    HStack {
                        Image(systemName: "phone")
                        TextField("Phone Number", text: $phoneNumber)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                            .keyboardType(.phonePad)
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                
                Section {
                    HStack {
                        Spacer()
                        Button(action: {
                            dependantType = getPickerValue(selectedIndex: selectedDependantType ?? 0, in: K.ArrayList.dependantList) ?? ""
                            let inputIsValid = self.phoneNumber.count == 11 && firstname.isEmpty == false && lastname.isEmpty == false
                            //trigger logic
                            if inputIsValid {
                                viewModel.addBeneficiary(firstName: firstname, lastName: lastname, phoneNumber: phoneNumber, type: dependantType)
                                let delay = DispatchTime.now() + .milliseconds(700)
                                DispatchQueue.main.asyncAfter(deadline: delay) {
                                    if viewModel.addBeneficiaryCode == 100 {
                                        self.fetchDependantData()
                                        clearInputView()
                                    } else {
                                        self.shouldShowLoginAlert = true //trigger Alert
                                    }
                                }
                            } else {
                                self.shouldShowLoginAlert = true //trigger Alert
                            }
                        }, label: {
                            Image(systemName: "person.fill.badge.plus")
                            Text("Add Another")
                        })
                        .foregroundColor(Color("rov-blue"))
                        .padding()
                        .font(.custom("Nunito-SemiBold", size: 13))
                        .alert(isPresented: $shouldShowLoginAlert) {
                            Alert(title: Text("OOPS!!!"), message: Text(errorText()))
                        }
                        .disabled(
                            numOfRegDependants >= 5
                        )
                    }
                }
                
                //MARK: - NAVIGATION LINKS
                Section {
                    NavigationLink(destination: ConfirmNewDependantsView(), isActive: $isDoneReg) {
                        Button(action: {
                            dependantType = getPickerValue(selectedIndex: selectedDependantType ?? 0, in: K.ArrayList.dependantList) ?? ""
                            let inputIsValid = self.phoneNumber.count == 11 && firstname.isEmpty == false && lastname.isEmpty == false
                            //trigger logic
//                            if numOfRegDependants < 5 {
                            if inputIsValid && numOfRegDependants < 5{
                                viewModel.addBeneficiary(firstName: firstname, lastName: lastname, phoneNumber: phoneNumber, type: dependantType)
                                let delay = DispatchTime.now() + .milliseconds(700)
                                DispatchQueue.main.asyncAfter(deadline: delay) {
                                    if viewModel.addBeneficiaryCode == 100 {
                                        self.isDoneReg = true //trigger NavigationLink
                                        self.fetchDependantData()
                                    } else {
                                        self.shouldShowLoginAlert = true //trigger Alert
                                    }
                                }
                            }
                            else {
                                self.isDoneReg = true
                            }
                        }, label: {
                            DefaultButtonView(buttonText: "Continue >>", textColor: .white, backgroundColor: Color("rov-blue"), borderColor: Color("rov-blue"))
                        })
                    }
                    .alert(isPresented: $shouldShowLoginAlert) {
                        Alert(title: Text("OOPS!!!"), message: Text(viewModel.addBeneficiaryMess))
                    }
                    
                    Button(action:  {
                        self.mode.wrappedValue.dismiss()
                    }) {
                        DefaultButtonView(buttonText: "I'll Do This Later", textColor: .gray, backgroundColor: .clear, borderColor: .clear)
                    }
                }
            }//ScrollView
            .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            Spacer()
        }//VStack
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .onAppear(perform: {
            showNumber()
        })
        .environmentObject(viewModel)
    }
}

struct AddNewDependantView_Previews: PreviewProvider {
    static var previews: some View {
        AddNewDependantView()
    }
}
