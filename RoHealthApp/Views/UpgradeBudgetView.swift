//
//  UpgradeBudgetView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/3/21.
//

import SwiftUI

struct UpgradeBudgetView: View {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @State var searchText: String = ""
    @State var isSearching: Bool = false
    @State var itemFound: Int = 0
//    let plans: [PlanModel] = Bundle.main.decode("plans.json")
    var plans: [GetPackagesDetails] = []
    @ObservedObject var viewModel = ViewModel()
    
    mutating func loadPackages() {
        viewModel.getPackages()
        plans = viewModel.getPackagesData
    }

    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            SearchBar(searchText: $searchText, isSearching: $isSearching)
                .padding(.vertical)
            Section {
                HStack {
                    Text("\(itemFound) results found")
                        .underline()
                    Spacer()
                    Text("1 of 20 pages")
                }
                .foregroundColor(Color("textColor"))
                .font(.custom("Nunito-Regular", size: 12))
                .padding()
            }
            Section {
                ForEach (plans.filter({"\($0)".contains(searchText) || searchText.isEmpty})) { plan in
                    NavigationLink(destination: UpgradePlanDetailsView(plan: plan)) {
                        BudgetListItem(plan: plan)
                    }
                }
                .padding()
            }
            
        }//ScrollView
        .navigationBarTitle("Upgrade Budget", displayMode: .inline)//.font(.custom("Nunito-SemiBold", size: 16))
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
                                Button(action: {
                                    self.mode.wrappedValue.dismiss()
                                }, label: {
                                    Image(systemName: "arrow.backward")
                                        .foregroundColor(Color("textColor"))
                                    
                                })
        )
    }
}

struct UpgradeBudgetView_Previews: PreviewProvider {
    static var previews: some View {
        UpgradeBudgetView()
    }
}
