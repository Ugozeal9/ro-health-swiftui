//
//  SingleDependantListView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/24/21.
//

import SwiftUI

struct SingleDependantListView: View {
    @ObservedObject var viewModel = ViewModel()
    var dependant: Dependent
    var body: some View {
        HStack(alignment: .center, spacing: 16)  {
            VStack(alignment: .leading, spacing: 8) {
                Text("\(dependant.firstName ?? "Lekan") \(dependant.lastName ?? "")")
                    .font(.custom("Nunito-Bold", size: 13))
                Text("Type: \(dependant.type ?? "Husband")")
                    .font(.custom("Nunito-Regular", size: 10))
                    .foregroundColor(Color("rov-blue"))
            }
            Spacer()
            Text("\(dependant.mobile ?? "00978979767")")
                .font(.custom("Nunito-Regular", size: 13))
            Spacer()
            Button(action: {
                print("Delete")
            }) {
                Image("delete-image")
                    .resizable()
                    .scaledToFill()
                    .frame(width:16, height: 16)
            }
        }
    }
}

struct SingleDependantListView_Previews: PreviewProvider {
    static var dependants: [Dependent] = ViewModel().getBeneficiaryData
    static var previews: some View {
        SingleDependantListView(dependant: dependants[0])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
