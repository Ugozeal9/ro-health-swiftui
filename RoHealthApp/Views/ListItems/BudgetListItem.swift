//
//  BudgetListItem.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/3/21.
//

import SwiftUI

struct BudgetListItem: View {
    var plan: GetPackagesDetails
    var planImage = ""
    var planColor = ""
    
    mutating func getPlanData() {
        if plan.id == 1 {
            planImage = "bronze-image"
            planColor = "bronze-color"
        } else if plan.id == 2 {
            planImage = "silver-image"
            planColor = "silver-color"
        } else if plan.id == 3 {
            planImage = "gold-image"
            planColor = "gold-color"
        } else {
            planImage = "platinum-image"
            planColor = "textColor"
        }
    }

    var body: some View {
        GroupBox {
            HStack(alignment: .center, spacing: 16) {
                Image(planImage)
                    .resizable()
                    .scaledToFill()
                    .frame(width:50, height: 50)
                    .clipShape(
                        RoundedRectangle(cornerRadius: 10)
                    )
                VStack(alignment: .leading, spacing: 10) {
                    Text(plan.name ?? String())
                        .font(.custom("Nunito-SemiBold", size: 13))
                        .foregroundColor(Color(planColor))
                    Text("\(plan.detailDescription?.count ?? Int()) Covers | \(plan.hospital ?? Int()) Hospitals")
                        .font(.custom("Nunito-Bold", size: 10))
                        .foregroundColor(Color("textColor"))
                }
                Spacer()
                VStack(alignment: .leading, spacing: 8) {
                    Text("view all")
                        .underline()
                        .font(.custom("Nunito-Bold", size: 11))
                        .foregroundColor(Color("textColor"))
                }
            }
        }
    }
}

struct BudgetListItem_Previews: PreviewProvider {
//    static var plans: [PlanModel] = Bundle.main.decode("plans.json")
    static var plans: [GetPackagesDetails] = ViewModel().getPackagesData
    static var previews: some View {
        BudgetListItem(plan: plans[0])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
