//
//  PlanCoverItem.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/25/21.
//

import SwiftUI

struct PlanCoverItem: View {
    var plan: GetPackagesDetails
    var body: some View {
        VStack(alignment: .leading) {
            ScrollView {
                ForEach(plan.detailDescription ?? [String](), id: \.self) { item in
                    HStack(alignment: .top)  {
                        Circle()
                            .foregroundColor(.gray)
                            .frame(width: 6, height: 6)
                            .padding(.top, 6)
                        Text(item)
                            .font(.custom("Nunito-SemiBold", size: 12))
                            
                        Spacer()
                    }
                }
                
            }
        }
        .padding(.leading)
    }
}

struct PlanCoverItem_Previews: PreviewProvider {
    static var plans: [PlanModel] = Bundle.main.decode("plans.json")
    static var packages: [GetPackagesDetails] = ViewModel().getPackagesData
    static var previews: some View {
        PlanCoverItem(plan: packages[0])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
