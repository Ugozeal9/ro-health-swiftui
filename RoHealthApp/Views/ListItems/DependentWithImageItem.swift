//
//  DependentWithImage.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/2/21.
//

import SwiftUI

struct DependentWithImageItem: View {
    var dependant: Dependent
    @StateObject var viewModel = ViewModel()
    var body: some View {
        HStack(alignment: .center, spacing: 16) {
            Image("gold-image")
                .resizable()
                .scaledToFill()
                .frame(width:50, height: 50)
                .clipShape(
                    RoundedRectangle(cornerRadius: 10)
                )
                .padding(.horizontal, 2)
            VStack(alignment: .leading) {
                Text("\(dependant.firstName ?? "") \(dependant.lastName ?? "")")
                    .font(.custom("Nunito-SemiBold", size: 12))
                    .foregroundColor(Color("textColor"))
                Text(dependant.type ?? "")
                    .font(.custom("Nunito-SemiBold", size: 14))
                    .foregroundColor(Color("bronze-color"))
                Text(dependant.mobile ?? "")
                    .font(.custom("Nunito-SemiBold", size: 10))
                    .foregroundColor(Color(.systemGray2))
            }
            Spacer()
            HStack(spacing: 8) {
                VStack {
                    Text("Swipe to Delete")
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .scaledToFill()
                        .frame(width:14, height: 14)
                        .padding(.horizontal, 2)
                }
            }
            .foregroundColor(.red)
            .font(.custom("Nunito-SemiBold", size: 10))
//            Button(action: {
//                viewModel.deleteBeneficiary(id: dependant.id ?? Int())
//                let delay = DispatchTime.now() + .milliseconds(700)
//                DispatchQueue.main.asyncAfter(deadline: delay) {
//                    if viewModel.deleteUserCode == 100 {
//                        viewModel.getBeneficiaries()
//                    }
//                }
//            }, label: {
//                VStack {
//                    Image(systemName: "arrow.backward")
//                        .resizable()
//                        .scaledToFill()
//                        .frame(width:14, height: 14)
//                        .padding(.horizontal, 2)
//                }
//            })
            
        }
        .padding()
        .background(
            RoundedRectangle(
                cornerRadius: 10
            )
            .foregroundColor(Color.white)
            .shadow(
                color: Color(.systemGray3),
                radius: 5,
                x: 0,
                y: 0
            )
        )
    }
}

struct DependentWithImage_Previews: PreviewProvider {
    static var dependants: [Dependent] = ViewModel().getBeneficiaryData
    static var previews: some View {
        DependentWithImageItem(dependant: dependants[0])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
