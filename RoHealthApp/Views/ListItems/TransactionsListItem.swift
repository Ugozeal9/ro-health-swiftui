//
//  TransactionsListItem.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/3/21.
//

import SwiftUI

struct TransactionsListItem: View {
    var transaction: Transaction
     //MARK: - Date Formatter
    func customDateFormatter(date: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a, MMMM dd, yyyy"
        let date: Date? = dateFormatterGet.date(from: date)
        return dateFormatter.string(from: date!)
    }
    
    func currencyFormatter(stringAmount: String) -> String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.currency
        // localize to your grouping and decimal separator
        currencyFormatter.locale = Locale(identifier: "en_NG")
        let number = UInt(stringAmount)
        let amount = NSNumber(value: number ?? UInt())
        let amountString = currencyFormatter.string(from: amount)
        return amountString ?? String()
    }

    var body: some View {
        HStack(alignment: .center, spacing: 16) {
            Image("gold-image")
                .resizable()
                .scaledToFill()
                .frame(width:50, height: 50)
                .clipShape(
                    RoundedRectangle(cornerRadius: 10)
                )
                .padding(.horizontal, 2)
            VStack(alignment: .leading, spacing: 10) {
                Text(transaction.info ?? String())
                    .font(.custom("Nunito-SemiBold", size: 12))
                    .foregroundColor(Color("textColor"))
                Text(customDateFormatter(date: transaction.created ?? String()))
                    .font(.custom("Nunito-SemiBold", size: 10))
                    .foregroundColor(Color(.systemGray2))
            }
            Spacer()
            Text(currencyFormatter(stringAmount: transaction.amount ?? String()))
                .font(.custom("Nunito-SemiBold", size: 10))
                .foregroundColor(Color(.systemGreen))
        }
        .padding()
        .background(
            RoundedRectangle(
                cornerRadius: 10
            )
            .foregroundColor(Color.white)
            .shadow(
                color: Color(.systemGray3),
                radius: 5,
                x: 0,
                y: 0
            )
        )
    }
}

struct TransactionsListItem_Previews: PreviewProvider {
    static var transactions: [Transaction] = ViewModel().getTransactionsData
    static var previews: some View {
        TransactionsListItem(transaction: transactions[0])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
