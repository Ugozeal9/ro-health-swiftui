//
//  UserPlanCoverItem.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 4/26/21.
//

import SwiftUI

struct UserPlanCoverItem: View {
    var plan: UserDetailsEntity
    var body: some View {
        VStack(alignment: .leading) {
            ScrollView {
                ForEach(plan.healthPlan?.healthPlanDescription ?? [String](), id: \.self) { item in
                    HStack(alignment: .top)  {
                        Circle()
                            .foregroundColor(.gray)
                            .frame(width: 6, height: 6)
                            .padding(.top, 6)
                        Text(item)
                            .font(.custom("Nunito-SemiBold", size: 12))
                            
                        Spacer()
                    }
                }
                
            }
        }
        .padding(.leading)
    }
}

struct UserPlanCoverItem_Previews: PreviewProvider {
    static var packages: UserDetailsEntity = ViewModel().getUserDetails
    static var previews: some View {
        UserPlanCoverItem(plan: packages)
    }
}
