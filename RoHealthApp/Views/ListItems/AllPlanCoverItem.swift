//
//  AllPlanCoverItem.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/4/21.
//

import SwiftUI

struct AllPlanCoverItem: View {
    var plan: GetPackagesDetails //PlanModel

    var body: some View {
        VStack(alignment: .leading) {
            ScrollView {
                ForEach(plan.detailDescription ?? [String](), id: \.self) { item in
                    HStack(alignment: .top)  {
                        Circle()
                            .foregroundColor(.gray)
                            .frame(width: 6, height: 6)
                            .padding(.top, 4)
                        Text(item)
                            .font(.custom("Nunito-SemiBold", size: 12))
                            
                        Spacer()
                    }
                }
                
            }
        }
        .padding(.leading)
    }
}

struct AllPlanCoverItem_Previews: PreviewProvider {
    static var plans: [PlanModel] = Bundle.main.decode("plans.json")
    static let packages: [GetPackagesDetails] = ViewModel().getPackagesData
    static var previews: some View {
        AllPlanCoverItem(plan: packages[0])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
