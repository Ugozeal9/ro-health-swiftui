//
//  HospitalListItem.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/26/21.
//

import SwiftUI

struct HospitalListItem: View {
    var hospital: HospitalEntity
    var body: some View {
        HStack (alignment: .center, spacing: 16)  {
            Image("gold-image")
                .resizable()
                .scaledToFill()
                .frame(width:50, height: 50)
                .clipShape(
                    RoundedRectangle(cornerRadius: 10)
                )
                .padding(.horizontal, 2)
            
            VStack(alignment: .leading) {
                Text(hospital.name ?? String())
                    .font(.custom("Nunito-SemiBold", size: 13))
                Text(hospital.address ?? String())
                    .font(.custom("Nunito-Regular", size: 10))
                    .foregroundColor(.accentColor)
                Text(hospital.mobile ?? String())
                    .font(.custom("Nunito-Regular", size: 10))
                    .foregroundColor(Color("textColor"))
                
            }
            Spacer()
        }
        .padding()
        .background(Color("hospital-bg"))
    }
}

struct HospitalListItem_Previews: PreviewProvider {
    static var hospitals: [HospitalEntity] = ViewModel().getHospitalData
    static var previews: some View {
        HospitalListItem(hospital: hospitals[0])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
