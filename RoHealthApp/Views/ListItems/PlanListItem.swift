//
//  PlanListItem.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/25/21.
//

import SwiftUI

struct PlanListItem: View {
    var package: GetPackagesDetails
    @State var planImage = ""
    @State var planColor = ""
    
    func getPlanData() {
        if package.name == "Bronze" {
            planImage = "bronze-image"
            planColor = "bronze-color"
        } else if package.name == "Silver" {
            planImage = "silver-image"
            planColor = "silver-color"
        } else if package.name == "Gold" {
            planImage = "gold-image"
            planColor = "gold-color"
        } else {
            planImage = "platinum-image"
            planColor = "textColor"
        }
    }

    var body: some View {
        GroupBox {
            HStack(alignment: .center, spacing: 16) {
                Image(planImage)
                    .resizable()
                    .scaledToFill()
                    .frame(width:50, height: 50)
                    .clipShape(
                        RoundedRectangle(cornerRadius: 10)
                    )
                Text("\(package.name ?? String()) Package")
                    .font(.custom("Nunito-SemiBold", size: 15))
                    .foregroundColor(Color(planColor))
                Spacer()
                VStack(alignment: .leading, spacing: 8) {
                    Text("\(package.detailDescription?.count ?? Int()) Covers")
                        .font(.custom("Nunito-Bold", size: 12))
                        .foregroundColor(Color("textColor"))
                    Text("\(package.hospital ?? Int()) Hospitals")
                        .font(.custom("Nunito-Bold", size: 12))
                        .foregroundColor(Color("textColor"))
                }
            }
        }
        .onAppear {
            getPlanData()
        }
    }
}

struct PlanListItem_Previews: PreviewProvider {
//    static var plans: [PlanModel] = Bundle.main.decode("plans.json")
    static var packages: [GetPackagesDetails] = ViewModel().getPackagesData
    static var previews: some View {
        PlanListItem(package: packages[0])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
