//
//  HMOCardItem.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/4/21.
//

import SwiftUI

struct HMOCardItem: View {
    //MARK:- Properties
    let cardImage: [UIImage]
    //MARK:- BODY
    var body: some View {
        TabView {
            ForEach(cardImage, id: \.self) { item in
                Image(uiImage: item)
                    .scaledToFill()
                    .cornerRadius(10)
            }
        }
        .tabViewStyle(PageTabViewStyle())
    }
}

struct HMOCardItem_Previews: PreviewProvider {
    static var plans: [PlanModel] = Bundle.main.decode("plans.json")
    static var previews: some View {
        HMOCardItem(cardImage: [UIImage(named: "card-image")!])
            .previewLayout(.fixed(width: 400, height: 300))
    }
}
