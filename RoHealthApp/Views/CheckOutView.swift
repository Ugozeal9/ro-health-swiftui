//
//  CheckOutView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/26/21.
//

import SwiftUI

struct CheckOutView: View {

    let plan: GetPackagesDetails //PlanModel

    var body: some View {
        VStack {
            ScrollView(.vertical, showsIndicators: false) {
                Section {
                    Image("congrats-image")
                        .resizable()
                        .scaledToFill()
                        .frame(width: 193, height: 173, alignment: .center)
                }
                .padding(EdgeInsets(top: 30, leading: 0, bottom: 0, trailing: 0))
                Section {
                    Text("Congrats!!!")
                        .font(.custom("Nunito-Bold", size: 30))
                        .foregroundColor(Color("textColor"))
                }
                .padding(EdgeInsets(top: 30, leading: 0, bottom: 0, trailing: 0))
                Section {
                    Text("You have now been enrolled under Bronze package insurance with Rovedana HMO. \n\nPlease note that your HMO would be active in \(Text("10 working days.").foregroundColor(.accentColor)) ")
                        .font(.custom("Nunito-Regular", size: 20))
                        .foregroundColor(Color("textColor"))
                }
                .padding()
                Section {
                    NavigationLink(destination: MainView()) {
                        HStack {
                         Text("Continue To Homepage")
                            .font(.custom("Nunito-SemiBold", size: 12))
                            .foregroundColor(Color("rov-blue"))
                            .padding()
                        }
                        .padding(.vertical, 10)
                        .background(Color("budget-Color"))
                        .cornerRadius(10)
                        .padding()
                    }
                }
            }
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct CheckOutView_Previews: PreviewProvider {
    static let plans: [PlanModel] = Bundle.main.decode("plans.json")
    static let packages: [GetPackagesDetails] = ViewModel().getPackagesData
    static var previews: some View {
        CheckOutView(plan: packages[0])
    }
}
