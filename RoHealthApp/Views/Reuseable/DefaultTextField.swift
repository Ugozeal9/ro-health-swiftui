//
//  DefaultTextField.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/22/21.
//

import SwiftUI

struct DefaultTextField: View {
    let image: String
    let placeholder: String
    @State var input: String
        
        var body: some View {
            HStack {
                Image(systemName: image)
                TextField(placeholder, text: $input, onEditingChanged: { (changed) in
                    
                })
                    .font(Font.custom("Nunito-SemiBold", size: 12))
                    
            }
            .padding()
            .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
            .padding()
        }
}

struct DefaultTextField_Previews: PreviewProvider {
    static var previews: some View {
        DefaultTextField(image: "person", placeholder: "Enter your Email or Phone Number", input: "")
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
