//
//  SearchBar.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/25/21.
//

import SwiftUI

struct SearchBar: View {
    @Binding var searchText: String
    @Binding var isSearching: Bool
    var body: some View {
        HStack {
            HStack {
                TextField("search by ailment, location etc ...", text: $searchText)
                    .font(.custom("Nunito-Regular", size: 12))
                    .padding(.leading, 24)
            }
            .padding()
//            .background(Color(.systemGray6))
//            .cornerRadius(10)
            .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
//            .padding()
            .padding(.horizontal)
            .onTapGesture(perform: {
                isSearching = true
            })
            .overlay(
                HStack {
                    Image(systemName: "magnifyingglass")
                        .foregroundColor(.gray)
                        .padding(.horizontal, 32)
                    Spacer()
                    if isSearching {
                        Button(action: {
                            searchText = ""
                        }) {
                            Image("search-button")
                                .padding(.horizontal, 10)
                        }
                        
                    }
                }
            )
            .transition(.move(edge: .trailing))
            .animation(.spring())
            if isSearching {
                Button(action: {
                    isSearching = false
                    searchText = ""
                    UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                }) {
                    Text("Cancel")
                        .font(.custom("Nunito-Bold", size: 14))
                        .padding(.trailing)
                        .padding(.leading, 0)
                }
                .transition(.move(edge: .trailing))
                .animation(.spring())
            }
          
        }

       
    }
}

//struct SearchBar_Previews: PreviewProvider {
//    static var previews: some View {
//        SearchBar(searchText: String = "", isSearching: Bool = false)
//            .previewDevice("iPhone 12 Pro")
//    }
//}
