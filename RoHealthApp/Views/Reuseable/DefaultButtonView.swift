//
//  DefaultButtonView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/20/21.
//

import SwiftUI

struct DefaultButtonView: View {
    let buttonText: String
    let textColor: Color
    let backgroundColor: Color
    let borderColor: Color
    
    var body: some View {
        HStack {
              HStack {
                  Text(buttonText)
                    .foregroundColor(textColor)
                    .font(.custom(K.Fonts.semiBold, fixedSize: 12))
                  }
              .frame(minWidth: 0, maxWidth: 370)
              .padding()
              .overlay(
                  RoundedRectangle(cornerRadius: 10)
                    .stroke(borderColor, lineWidth: 1)
              )
              .background(RoundedRectangle(cornerRadius: 10).fill(backgroundColor))

              }
        .padding()
    }
}

struct DefaultButtonView_Previews: PreviewProvider {
    static var previews: some View {
        DefaultButtonView(buttonText: "Returning User? Login", textColor: .gray, backgroundColor: .white, borderColor: .gray)
            .previewDevice("iPhone 7")
//            .preferredColorScheme(.dark)
//            .previewLayout(.sizeThatFits)
//            .padding()
    }
}
