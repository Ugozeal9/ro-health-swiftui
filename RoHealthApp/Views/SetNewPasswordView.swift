//
//  SetNewPasswordView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/26/21.
//

import SwiftUI

struct SetNewPasswordView: View {
    @State var newPassword: String = ""
    @State var confirmNewPassword: String = ""
    @State var isInputFieldValid: Bool = false
    @State private var shouldShowLoginAlert: Bool = false
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @ObservedObject var viewModel = ViewModel()
    
    func getID() {
        print(viewModel.forgotPasswordRef, ">>")
    }
    
    var body: some View {
        VStack {
            TopView()
                .frame(height: 170)
                .edgesIgnoringSafeArea(.all)
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            ScrollView {
                Section {
                    Text("Enter OTP to set new Password")
                        .font(.custom(K.Fonts.semiBold, fixedSize: 18))
                        .foregroundColor(.black)
                }
                
                Section {
                    HStack {
                        SecureField("Enter new Password", text: $newPassword)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
                
                Section {
                    HStack {
                        SecureField("Confirm new Password", text: $confirmNewPassword)
                            .font(Font.custom("Nunito-SemiBold", size: 12))
                    }
                    .frame(minWidth: 0, maxWidth: 370)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 0.5).foregroundColor(Color.gray))
                    .padding()
                }
              
                Section {
                    NavigationLink(destination: LoginView(), isActive:  (self.$isInputFieldValid)) {
                        Button(action: {
                            let notRegistered = self.newPassword.count == 4 && self.confirmNewPassword.count == 4 && self.newPassword == self.confirmNewPassword
                            //trigger logic
                            if notRegistered {
                                viewModel.setNewPassword(password: newPassword, confirmPassword: confirmNewPassword)
                                self.isInputFieldValid = true //trigger NavigationLink
                            } else {
                                self.shouldShowLoginAlert = true //trigger Alert
                            }
                        }, label: {
                            DefaultButtonView(buttonText: "Submit", textColor: .white, backgroundColor: Color("rov-blue"), borderColor: Color("rov-blue"))
                        })

                    } .alert(isPresented: $shouldShowLoginAlert) {
                        Alert(title: Text("OOPS!!!"), message: Text(viewModel.resetPasswordMess))
                    }
                }
                .disabled(newPassword.count != 4 && confirmNewPassword.count != 4 && newPassword != confirmNewPassword)
                Section {
                    
                    Button(action:  {
                        self.mode.wrappedValue.dismiss()
                    }) {
                        DefaultButtonView(buttonText: "Didn't get OTP?", textColor: .black, backgroundColor: .clear, borderColor: .clear)
                    }
                }
            }
            Spacer()
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct SetNewPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        SetNewPasswordView()
    }
}
