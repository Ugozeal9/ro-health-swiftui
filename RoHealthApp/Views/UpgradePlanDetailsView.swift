//
//  UpgradePlanDetailsView.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 3/3/21.
//

import SwiftUI

struct UpgradePlanDetailsView: View {
    //MARK:- Properties
    @State var searchText = ""
    @State var isSearching = false
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    let plan: GetPackagesDetails //PlanModel

    var body: some View {
        VStack {
            ScrollView(.vertical, showsIndicators: false) {
                ZStack {
                    Image("onboarding1-image")
                        .resizable()
                        .scaledToFill()
                        .frame(width: 250)
                    VStack{
                        HStack {
                          Text("Based on your Employer\nset Budget, You will be Adding:")
                            .font(.custom("Nunito-Regular", size: 12))
                            .foregroundColor(.white)
                            Spacer()
                            VStack(alignment: .leading, spacing: 5) {
                                Text(plan.price ?? String())
                                    .font(.custom("Nunito-SemiBold", size: 30))
                                    .foregroundColor(Color("bronze-color"))
                                Text("Per Quarter")
                                    .font(.custom("Nunito-SemiBold", size: 13))
                                    .foregroundColor(.white)
                            }
                        }
                        .padding(.vertical, 5)
                        Spacer()
                        NavigationLink(destination: SuccessfulPaymentView(plan: plan)) {
                            HStack {
                             Text("Purchase Plan")
                                .font(.custom("Nunito-SemiBold", size: 12))
                                .foregroundColor(Color("rov-blue"))
                                .padding(.vertical)
                            }
                            .padding(.horizontal, 50)
                            .background(Color.white)
                            .cornerRadius(10)
                            .padding()
                        }.simultaneousGesture(TapGesture().onEnded{
                            print("Bought")
                        })
                    }
                    .padding()
                }//:ZSTACK
                .frame(height: 195)
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.clear, lineWidth: 1)
                )
                .background(RoundedRectangle(cornerRadius: 10).fill(Color("rov-blue")))
                .padding()
               
                VStack(alignment: .center, spacing: 20) {

                    HStack {
                        Text("Plan Covers: ")
                            .underline()
                            .foregroundColor(Color("bronze-color"))
                            .font(.custom("Nunito-SemiBold", size: 14))
                        Spacer()
                        NavigationLink(destination: AllPlanDetailsView(plan: plan)) {
                            Text("View All")
                                .foregroundColor(Color("rov-blue"))
                                .font(.custom("Nunito-SemiBold", size: 11))
                        }
                    }
                    .padding()
                    PlanCoverItem(plan: plan)
                    Section {
                        NavigationLink(destination: SelectHospitalView()) {
                            HStack {
                                Text("Search Hospitals That You Can use This Plan At:")
                                    .underline()
                                    .foregroundColor(Color("bronze-color"))
                                    .font(.custom("Nunito-SemiBold", size: 14))
                                Spacer()
                            }
                            .padding()
                        }
                    }
                    
                    SearchBar(searchText: $searchText, isSearching: $isSearching)
                   Spacer()
                    
                }//VSTACK
            }
        }
        .navigationBarTitle("\(plan.name ?? String()) Plan", displayMode: .inline)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
                                Button(action: {
                                    self.mode.wrappedValue.dismiss()
                                }, label: {
                                    Image(systemName: "arrow.backward")
                                        .foregroundColor(Color("textColor"))
                                    
                                })
        )
    }
}

struct UpgradePlanDetailsView_Previews: PreviewProvider {
    static let plans: [PlanModel] = Bundle.main.decode("plans.json")
    static var packages: [GetPackagesDetails] = ViewModel().getPackagesData
    static var previews: some View {
        UpgradePlanDetailsView(plan: packages[0])
    }
}
