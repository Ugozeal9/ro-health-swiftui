//
//  StringConstants.swift
//  RoHealthApp
//
//  Created by David U. Okonkwo on 2/20/21.
//

import Foundation

enum K {
    enum Fonts {
        static let semiBold = "Nunito-SemiBold"
        static let black = "Nunito-Black"
        static let blackItalic = "Nunito-BlackItalic"
        static let regular = "Nunito-Regular"
        static let light = "Nunito-Light"
        static let lightItalic = "Nunito-LightItalic"
        static let italic = "Nunito-Italic"
        static let extraLightItallic = "Nunito-ExtraLightItallic"
        static let extraLight = "Nunito-ExtraLight"
        static let extraBoldItallic = "Nunito-ExtraBoldItallic"
        static let extraBold = "Nunito-ExtraBold"
        static let boldItallic = "Nunito-BoldItallic"
        static let bold = "Nunito-Bold"
    }
    
    enum ArrayList {
        static let genderList = ["Male", "Female"]
        static let dependantList = ["Child", "Spouse"]
        static let maritalStatusList = ["Single", "Married", "Divorced", "Widowed"]
        static let stateList: [String] = [
            "Abia",
            "Adamawa",
            "Akwa Ibom",
            "Anambra",
            "Bauchi",
            "Bayelsa",
            "Benue",
            "Borno",
            "Cross River",
            "Delta",
            "Ebonyi",
            "Edo",
            "Ekiti",
            "Enugu",
            "FCT - Abuja",
            "Gombe",
            "Imo",
            "Jigawa",
            "Kaduna",
            "Kano",
            "Katsina",
            "Kebbi",
            "Kogi",
            "Kwara",
            "Lagos",
            "Nasarawa",
            "Niger",
            "Ogun",
            "Ondo",
            "Osun",
            "Oyo",
            "Plateau",
            "Rivers",
            "Sokoto",
            "Taraba",
            "Yobe",
            "Zamfara"
          ]
        static let countryList = Locale.isoRegionCodes.compactMap { Locale.current.localizedString(forRegionCode: $0) }
    }
    enum Url {
        static let BASE_URL = "https://rohealth.ng"
        static let SIGN_UP = BASE_URL + "api/account/signup"
        static let LOG_IN = BASE_URL + "api/account/login"
        static let FORGOT_PASSWORD = BASE_URL + "api/account/forgot-password"
        static let ADD_BENEFICIARY = BASE_URL + "api/add-beneficiary"
        static let VERIFY_PHONENUMBER = BASE_URL + "api/account/verify"
        static let VERIFY_OTP = BASE_URL + "api/account/authenticate"
        static let RESET_PASSWORD = BASE_URL + "api/account/reset-password?ref="
        static let GET_BENEFICIARIES = BASE_URL + "api/beneficiaries"
        static let DELETE_BENEFICIARY = BASE_URL + "api/beneficiary/"
        static let GET_PACKAGES = BASE_URL + "api/packages"
        static let PURCHASE_PACKAGE = BASE_URL + "api/purchase/package/"
        static let CONFIRM_BENEFICIARIES = BASE_URL + "api/confirm-beneficiaries"
        static let GET_NEW_BENEFICIARIES = BASE_URL + "api/new/beneficiaries"
        static let GET_HOSPITALS = BASE_URL + "api/hospitals"
        static let GET_USER_DETAILS = BASE_URL + "api/auth/user"
        static let LOG_OUT = BASE_URL + "api/account/logout"
        static let GET_TRANSACTIONS = BASE_URL + "api/transactions"
    }
}
